const oneLine = require('common-tags').oneLine;
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'ping',
			group: 'util',
			memberName: 'ping',
			description: 'Checks the bot\'s ping to the Discord server.',
			throttling: {
				usages: 5,
				duration: 20
			}
		});
	}

	async run(msg) {
		if(!msg.editable) {
			const pingMsg = await msg.reply('Pinging...');
			
			var ping = Math.round(this.client.ping);
			
			if (ping < 50)
			{
				ping = "Fast";
			}
			else if (ping > 50 && ping <= 75)
			{
				ping = "Normal";
			}
			else if (ping > 75 && ping < 300)
			{
				ping = "Slow";
			}
			else
			{
				ping = "Terrible";
			}

			var content = this.client.CreateEmbed(msg.channel, `Pong! Status: ${ping}`, [{ title : `Heartbeat Ping`, description: `${Math.round(this.client.ping)}ms` }, { title : `Message Roundtrip`, description: `${pingMsg.createdTimestamp - msg.createdTimestamp}ms` }], msg);
			console.log(typeof content);

			if (typeof content == "string")
			{
				return pingMsg.edit(content);
			}
			else
			{
				return pingMsg.edit({ embed : content });
			}
		} else {
            await msg.edit('Pinging...');
			
			var ping = Math.round(this.client.ping);
			
			if (ping < 50)
			{
				ping = "Fast";
			}
			else if (ping > 50 && ping <= 75)
			{
				ping = "Normal";
			}
			else if (ping > 75 && ping < 300)
			{
				ping = "Slow";
			}
			else
			{
				ping = "Terrible";
			}

			var content = this.client.CreateEmbed(msg.channel, `Pong! Status: ${ping}`, [{ title : `Heartbeat Ping`, description: `${Math.round(this.client.ping)}ms` }, { title : `Message Roundtrip`, description: `${pingMsg.createdTimestamp - msg.createdTimestamp}ms` }])
			console.log(typeof content);

			if (typeof content == "string")
			{
				return msg.edit(content);
			}
			else
			{
				return msg.edit({ embed : content });
			}
		}
	}
};
