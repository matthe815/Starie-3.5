const util = require('util');
const discord = require('discord.js');
const tags = require('common-tags');
const escapeRegex = require('escape-string-regexp');
const Command = require('discord.js-commando').Command;

const nl = '!!NL!!';
const nlPattern = new RegExp(nl, 'g');

module.exports = class EvalCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'pings',
			group: 'util',
			memberName: 'pings',
			autoAliases: true,
			description: 'Get the pings of all Clients.',
			ownerOnly: true,
			details: 'Only the bot owner(s) may use this command.',
		});

		this.lastResult = null;
	}

	hasPermission(msg) {
		return this.client.isOwner(msg.author);
	}

	async run(msg, args) {
        var pings = await this.client.shard.fetchClientValues("ping");
        var pingList = [];
        var i=0;

        pings.forEach((ping) => {
            i++;
            pingList.push({title: `Shard ${i} ${GetSmiley(ping)}`, description: ping });
        }); 
            
		var content = this.client.CreateEmbed(msg.channel, ``, pingList, msg);
		console.log(typeof content);

		if (typeof content == "string")
		{
			return msg.channel.send(content);
		}
		else
		{
			return msg.channel.send({ embed : content });
		}
	}
};

function GetSmiley(ping)
{
	var smilies = [":smile:", ":expressionless:", ":slight_frown:", ":frowning:", ":angry:"];

	if (ping < 40)
	{
		return smilies[0];
	}
	else if (ping > 40 && ping < 80)
	{
		return smilies[1];
	}
	else if (ping > 80 && ping < 120)
	{
		return smilies[2];
	}
	else if (ping > 120 && ping < 150)
	{
		return smilies[3];
	}
	else if (ping > 150)
	{
		return smilies[4];
	}
}