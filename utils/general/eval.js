const util = require('util');
const discord = require('discord.js');
const tags = require('common-tags');
const escapeRegex = require('escape-string-regexp');
const Command = require('discord.js-commando').Command;

const nl = '!!NL!!';
const nlPattern = new RegExp(nl, 'g');

module.exports = class EvalCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'eval',
			group: 'util',
			memberName: 'eval',
			autoAliases: true,
			description: 'Executes Any Arbitrary JavaScript code.',
			ownerOnly: true,
			details: 'Only the bot owner(s) may use this command.',

			args: [
				{
					key: 'script',
					prompt: 'What code would you like to evaluate?',
					type: 'string'
				}
			]
		});

		this.lastResult = null;
	}

	hasPermission(msg) {
		return this.client.isOwner(msg.author);
	}

	async run(msg, args) {
		// Make a bunch of helpers
		/* eslint-disable no-unused-vars */
		const message = msg;
		const client = msg.client;
		var silent = args.script.endsWith("--silent");
		if (silent) args.script = args.script.slice(args.script.length, -8);
		const objects = client.registry.evalObjects;
		const lastResult = this.lastResult;
		const doReply = val => {
			if(val instanceof Error) {
				msg.reply(`Callback error: \`${val}\``);
			} else {
				const result = this.makeResultMessages(val, process.hrtime(this.hrStart));
				if(Array.isArray(result)) {
					for(const item of result) {
						if(this.client.options.selfbot) msg.say(item); else { var emb = new discord.RichEmbed();emb.setDescription(`\`\`\`js ${result}\`\`\``); emb.setTitle(msg.author.username, msg.author.avatarURL); emb.setFooter(`Executed in ${process.hrtime(this.hrStart)}ms`); emb.setTimestamp(new Date()); msg.channel.send({ embed : emb }); }
					}
				} else if(this.client.options.selfbot) {
					msg.say(result);
				} else {
					var emb = new discord.RichEmbed();
					emb.setDescription(`\`\`\`js ${result}\`\`\``);
					emb.setTitle(msg.author.username, msg.author.avatarURL);
					emb.setFooter(`Executed in ${process.hrtime(this.hrStart)[1] / 1000}ms`);
					emb.setTimestamp(new Date());
					msg.channel.send({ embed : emb });
				}
			}
		};
		/* eslint-enable no-unused-vars */

		// Run the code and measure its execution time
		let hrDiff;
		try {
			const hrStart = process.hrtime();
			this.lastResult = eval(args.script);
			hrDiff = Math.floor(process.hrtime(hrStart)[1]) / 1000;
		} catch(err) {
			var emb = new discord.RichEmbed();
			emb.setDescription(err);
			emb.addField("Code", `\`\`\`js ${args.script} \`\`\``);
			emb.setTitle("Evaluation Error");
			emb.setTimestamp(new Date());
			return msg.channel.send({ embed : emb });
		}

		// Prepare for callback time and respond
		this.hrStart = process.hrtime();
		let response = this.makeResultMessages(this.lastResult, hrDiff, args.script, msg.editable);
		if(msg.editable && !silent) {
			if(response instanceof Array) {
				if(response.length > 0) response = response.slice(1, response.length - 1);
				for(const re of response) msg.say(re);
				return null;
			} else {
				var emb = new discord.RichEmbed();
				emb.addField("Input", `\`\`\`js ${args.script}\`\`\``);
				emb.addField("Response", `\`\`\`js ${response}\`\`\``);
				emb.setTitle(msg.author.username, msg.author.avatarURL);
				emb.setFooter(`Executed in ${hrDiff / 1000}ms`);
				emb.setTimestamp(new Date());
				return msg.edit({ embed : emb });
			}
		} else if (!silent) {
			var emb = new discord.RichEmbed();
			emb.addField("Input", tags.stripIndents`
			\`\`\`js
			${args.script} 
			\`\`\``);
			emb.addField("Response", response);
			emb.setTitle(msg.author.username, msg.author.avatarURL);
			emb.setFooter(`Executed in ${hrDiff / 1000}ms`);
			emb.setTimestamp(new Date());
			return msg.channel.send({ embed: emb });
		}
	}

	makeResultMessages(result, hrDiff, input = null, editable = false) {
		const inspected = util.inspect(result, { depth: 0 })
			.replace(nlPattern, '\n')
			.replace(this.sensitivePattern, '--snip--');
		const split = inspected.split('\n');
		const last = inspected.length - 1;
		const prependPart = inspected[0] !== '{' && inspected[0] !== '[' && inspected[0] !== "'" ? split[0] : inspected[0];
		const appendPart = inspected[last] !== '}' && inspected[last] !== ']' && inspected[last] !== "'" ?
			split[split.length - 1] :
			inspected[last];
		const prepend = `\`\`\`javascript\n${prependPart}\n`;
		const append = `\n${appendPart}\n\`\`\``;
		if(input) {
			return discord.splitMessage(tags.stripIndents`
				${editable ? `
					*Input*
					\`\`\`javascript
					${input}
					\`\`\`` :
				''}
				\`\`\`javascript
				${inspected}
				\`\`\`
			`, 1900, '\n', prepend, append);
		} else {
			return discord.splitMessage(tags.stripIndents`
				\`\`\`javascript
				${inspected}
				\`\`\`
			`, 1900, '\n', prepend, append);
		}
	}

	get sensitivePattern() {
		if(!this._sensitivePattern) {
			const client = this.client;
			let pattern = '';
			if(client.token) pattern += escapeRegex(client.token);
			if(client.email) pattern += (pattern.length > 0 ? '|' : '') + escapeRegex(client.email);
			if(client.password) pattern += (pattern.length > 0 ? '|' : '') + escapeRegex(client.password);
			Object.defineProperty(this, '_sensitivePattern', { value: new RegExp(pattern, 'gi') });
		}
		return this._sensitivePattern;
	}
};