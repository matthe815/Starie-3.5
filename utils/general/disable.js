const { oneLine, stripIndents } = require('common-tags');
const Command = require('discord.js-commando').Command;
const disambiguation = require('discord.js-commando').util.disambiguation;
const Discord = require('discord.js');

module.exports = class DisableCommandCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'disable',
			aliases: ['disable-command', 'cmd-off', 'command-off'],
			group: 'commands',
			memberName: 'disable',
			description: 'Disables a command or command group.',
			details: oneLine`
				The argument must be the name/ID (partial or whole) of a command or command group.
				Only administrators may use this command.
			`,
			examples: ['disable util', 'disable Utility', 'disable prefix'],
			guarded: true,

			args: [
				{
					key: 'cmdOrGrp',
					label: 'command/group',
					prompt: 'Which command or group would you like to toggle?',
					validate: val => {
						if(!val) return false;
						const groups = this.client.registry.findGroups(val);
						if(groups.length === 1) return true;
						const commands = this.client.registry.findCommands(val);
						if(commands.length === 1) return true;
						if(commands.length === 0 && groups.length === 0) return false;
						return stripIndents`
							${commands.length > 1 ? disambiguation(commands, 'commands') : ''}
							${groups.length > 1 ? disambiguation(groups, 'groups') : ''}
						`;
					},
					parse: val => this.client.registry.findGroups(val)[0] || this.client.registry.findCommands(val)[0]
				}
			]
		});
	}

	hasPermission(msg) {
		if(!msg.guild) return this.client.isOwner(msg.author);
		return msg.member.hasPermission('ADMINISTRATOR') || this.client.isOwner(msg.author);
	}

	async run(msg, args) {
		if(!args.cmdOrGrp.isEnabledIn(msg.guild)) {
			var re = new Discord.RichEmbed();
			re.setAuthor(msg.guild.name, msg.guild.iconURL);
			re.setDescription(`The \`${args.cmdOrGrp.name}\` ${args.cmdOrGrp.group ? 'Command' : 'Group'} is Already Disabled.`);
			return msg.channel.send({ embed : re });
		}
		if (args.cmdOrGrp.guarded) {
			var re = new Discord.RichEmbed();
			re.setAuthor(msg.guild.name, msg.guild.iconURL);
			re.setDescription(`The \`${args.cmdOrGrp.name}\` ${args.cmdOrGrp.group ? 'Command' : 'Group'} is Guarded and Cannot Be Disabled.`);
			return msg.channel.send({ embed : re });
		}

		args.cmdOrGrp.setEnabledIn(msg.guild, false);
		var re = new Discord.RichEmbed();
		re.setAuthor(msg.guild.name, msg.guild.iconURL);
		re.setDescription(`Disabled the \`${args.cmdOrGrp.name}\` ${args.cmdOrGrp.group ? 'Command' : 'Group'}.`);
		return msg.channel.send({ embed : re });
	}
};
