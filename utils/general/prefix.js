const { stripIndents, oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class PrefixCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'prefix',
			group: 'general',
			memberName: 'prefix',
			description: 'Shows or sets the command prefix.',
			format: '[prefix/"default"/"none"]',
			details: oneLine`
				If no prefix is provided, the current prefix will be shown.
				If the prefix is "default", the prefix will be reset to the bot's default prefix.
				If the prefix is "none", the prefix will be removed entirely, only allowing mentions to run commands.
				Only administrators may change the prefix.
			`,
			examples: ['prefix', 'prefix -', 'prefix omg!', 'prefix default', 'prefix none'],

			args: [
				{
					key: 'prefix',
					prompt: 'What would you like to set the bot\'s prefix to?',
					type: 'string',
					max: 30,
					default: ''
				}
			]
		});
	}

	async run(msg, args) {
		// Just output the prefix
		if(!args.prefix) {
			const prefix = msg.guild ? msg.guild.commandPrefix : this.client.commandPrefix;

			var content = this.client.CreateEmbed(msg.channel, stripIndents`
			${prefix ? `The Command Prefix is \`${prefix}\`` : 'There is No Command Prefix'}
			To Run Commands, Use ${msg.anyUsage('command')}.
			 `, [], msg);
			console.log(typeof content);

			if (typeof content == "string")
			{
				return msg.channel.send(content);
			}
			else
			{
				return msg.channel.send({ embed : content });
			}
		}

		// Check the user's permission before changing anything
		if(msg.guild) {
			if(!msg.member.hasPermission('ADMINISTRATOR') && !this.client.isOwner(msg.author)) {		
				var re = new Discord.RichEmbed();
				re.setAuthor(msg.guild.name, msg.guild.iconURL);
				re.setDescription(`Only Administrators May Change the Guild Prefix`);
				return msg.channel.send({ embed : re });
			}
		} else if(!this.client.isOwner(msg.author)) {
			var re = new Discord.RichEmbed();
			re.setAuthor(msg.guild.name, msg.guild.iconURL);
			re.setDescription(`Only Bot Owners May Change the Global Prefix`);
			return msg.channel.send({ embed : re });
		}

		// Save the prefix
		const lowercase = args.prefix.toLowerCase();
		const prefix = lowercase === 'none' ? '' : args.prefix;
		let response;
		if(lowercase === 'default') {
			if(msg.guild) msg.guild.commandPrefix = null; else this.client.commandPrefix = null;
			const current = this.client.commandPrefix ? `\`${this.client.commandPrefix}\`` : 'no prefix';
			response = `Reset The Command Prefix to the Default (Currently ${current}).`;
		} else {
			if(msg.guild) msg.guild.commandPrefix = prefix; else this.client.commandPrefix = prefix;
			response = prefix ? `Set the Command Prefix to \`${args.prefix}\`.` : 'Removed The Command Prefix Entirely.';
		}

		var re = new Discord.RichEmbed();
		re.setAuthor(msg.guild.name, msg.guild.iconURL);
		re.setDescription(`${response} To Run Commands, Use ${msg.anyUsage('command')}.`);
		return msg.channel.send({ embed : re });

		return null;
	}
};
