const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class GetGuildCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'getuser',
			group: 'fetching',
			memberName: 'getuser',
			aliases: ["user","fetchuser","userinfo"],
			guildOnly: true,
			description: 'Get the user information for a user.',
			examples: ["getuser \"Matthe815\""],
			throttling: {
				usages: 2,
				duration: 10
			},
			args: [{ key: "user", label: "User", type: "user", prompt: "Please provide a user to search.", default: "" }]
		});
	}
	
	async run(msg, args) {
		if (args.user != "")
		{
			var user = args.user;
			var member = msg.guild.member(args.user);
		}
		else
		{
			var user = msg.author;
			var member = msg.guild.member(msg.member);
		}
	
			var userData = this.client.userData.get(user.id);
			var emb = new Discord.RichEmbed();		
			emb.setAuthor(`${user.username} [${user.id}] ${this.client.isOwner(user) ? "✅" : ""}`, user.displayAvatarURL);
			emb.addField("Tag", user.tag,true);
			if (member && msg.member)
			{
				emb.addField("Nickname", member.nickname ? member.nickname : "No Nickname Set",true);
			}
			if (userData)
			{
				emb.addField("Money", `$${userData.money ? userData.money : 0}`,true);
				emb.addField("Prefix", userData.prefix ? userData.prefix : "None Set", true);
			}
			if (member && msg.member && member.roles.size > 1)
			{
				emb.addField("Highest Role", member.highestRole, true);
			}
			
			if (userData)
			{
				var claimed = new Date().getTime() - userData.dailyTime < 86400000;
				emb.addField("Cashed Check Today", claimed ? "Yes" : "No");
				if (claimed)
				{
					var time = new Date(userData.dailyTime - new Date().getTime());
					emb.addField("Until New Check", `${time.getHours()} hours, ${time.getMinutes()} minutes, and ${time.getSeconds()} seconds`, true);
				}
			}
			if (member && msg.member && member.roles.size > 1)
			{
				emb.addField("Roles", member.roles.array().slice(1).join(", "))
			}
			emb.setThumbnail(user.displayAvatarURL);
			return await msg.channel.send({ embed: emb });
	}
};

function getGuild(value, client)
{
	if (client.guilds.has(value))
	{
		return client.guilds.get(value); 
	}
	else if (client.guilds.find("name", value))
	{
		return client.guilds.find("name", value)
	}
		
	return null;
}