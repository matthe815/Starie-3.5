const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class GetGuildCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'getguild',
			group: 'fetching',
			memberName: 'getguild',
			guildOnly: false,
			aliases: ["guild","fetchguild","guildinfo"],
			description: 'Get information about a specific guild, provide no arguments if you want to see your current guild.',
			examples: ["getguild \"Starie 3.0 R&D\"", "getguild"],
			throttling: {
				usages: 2,
				duration: 10
			},
			args: [{ key: "guild", label: "Guild", type: "guild", prompt: "Please provide a guild to search.", default: "" }]
		});
	}
	
	async run(msg, args) {
		if (args.guild == "" && !msg.guild) return msg.reply("You must be in a guild to use this command without an argument."); 
		if (args.guild != "")
		{
			var server = args.guild;
		}
		else
		{
			var server = msg.guild;
		}
		
		var members = server.members.filter((member) => {
			return member.user.bot == false;
		});

		var bots = server.members.filter((member) => {
			return member.user.bot == true;
		});

		var	mods = server.members.filter((member) => {
			return member.hasPermission("MANAGE_MESSAGES") && !member.user.bot; 
		});

		var botList = "";

		bots.forEach((bot) => {
			botList = botList + bot.user.username + ", ";
		});

		var emb = new Discord.RichEmbed();
		emb.setAuthor(`${server.name} ${this.client.guildData.get(server.id).verified ? "✅" : ""}`, server.iconURL);
		emb.setThumbnail(server.iconURL);
		emb.addField("Owner", server.owner);
		emb.addField("Members (Users)", server.memberCount - bots.size, true)
		emb.addField("Members (Moderators)", mods.size)
		emb.addField("Members (Bots)", bots.size, true);

		if (bots.size < 5)
		{
			emb.addField("Bots", bots.array().join(", "));
		}
		
		var notableMembers = members.filter((member) => {
			return this.client.isOwner(member.user);
		});
		
		if (notableMembers.size > 0)
		{
			emb.addField("Notable Members", notableMembers.array().join(", "));
		}

		if (mods.size > 0 && mods.size <= 5)
		{
			emb.addField("Moderators", mods.array().join(", "));
		}

		emb.addField("# Of Channels", server.channels.size, true);
		emb.addField("# Of Roles", server.roles.size, true);
		return await msg.channel.send({ embed : emb });
	}
};