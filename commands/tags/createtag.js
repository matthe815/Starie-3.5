const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const discord = require("discord.js");

module.exports = class CTCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'createtag',
			group: 'tags',
			memberName: 'createtag',
			guildOnly: true,
			description: 'Create a brand new guild-unique command.',
			examples: ["createtag 'hello' 'How are you?'"],
			args: [{ key: "tag", type: "string", prompt: "Please enter a command tag name.", label: "Tag Name"}, { key: "response", type: "string", prompt: "Please enter a response on usage of the command.", label: "Tag response"}]
		});
	}
	
	async run(msg, args) {
        var cckey = this.client.cCommands.get(msg.guild.id);

        // Base RE response.
        const RE = new discord.RichEmbed();
        RE.setAuthor(msg.guild.name, msg.guild.iconURL);
        RE.setTimestamp(new Date());
        
        if (this.client.cCommands.has(msg.guild.id) && cckey.has(args.tag)) { RE.setDescription("A Tag With That Key Already Exists!"); return msg.channel.send({ embed : RE }); }
        if (args.tag.indexOf(/ /g) != -1) { RE.setDescription("You Cannot Have Spaces in Your Tag!"); return msg.channel.send({ embed : RE }); }

        this.client.addCommand(args.tag, args.response, msg);

        RE.setDescription(`A Tag With the Key \`${args.tag}\` Has Been Recorded`);
        return msg.channel.send({ embed : RE });
	}
};