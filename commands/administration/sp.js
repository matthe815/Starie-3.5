const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class SPCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'sp',
			group: 'administration',
            memberName: 'sp',
            aliases: ['starieperms', 'stariepermissions'],
			guildOnly: true,
			ownerOnly: false,
			description: 'Enable/Disable Starie Permissions',
		});
	}
    
	hasPermission(msg)
	{
		return this.client.Differentiate("ADMINISTRATOR", "OWNER", msg.member) || this.client.isOwner(msg.author);
	}

	async run(msg, args) {
        var guild = this.client.guildData.get(msg.guild.id);
        guild.stariePerms = !Boolean(guild.stariePerms);
        
        this.client.provider.db.run("UPDATE `guildSettings` SET `stariePerms`=? WHERE `guild`=?", [guild.stariePerms, msg.guild.id]);

        var content = this.client.CreateEmbed(msg.channel, `Successfully Set Starie Permissions to ${guild.stariePerms}`, [], msg);
			
        if (typeof content == "string")
        {
            return msg.channel.send(content);
        }
        else
        {
            return msg.channel.send({ embed : content });
        }
	}
};