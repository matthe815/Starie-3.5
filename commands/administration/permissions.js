const isArray = require('util').isArray;
const discord = require('discord.js');
const Command = require('discord.js-commando').Command;

module.exports = class PruneCommmand extends Command {
	constructor(client) {
		super(client, {
			name: 'permissions',
			group: 'administration',
			memberName: 'permissions',
			aliases: ["permissionsfor","listpermissions"],
			autoAliases: true,
			description: 'List all permissions of a role or avaliable.',
			ownerOnly: false,
			throttling: {
				usages: 2,
				duration: 5
			},

			args: [
				{
					key: 'role',
					prompt: 'Which role would you like to modify?',
                    type: 'role',
                    default: ''
				}
			]
		});

	}

	hasPermission(msg)
	{
		return this.client.Differentiate("ADMINISTRATOR", "VIEW_PERMISSIONS", msg.member) || this.client.isOwner(msg.author);
	}

	async run(msg, args) {

        if (args.role != '' && !this.client.rolePermissions.has(args.role.id))
        {
            var content = this.client.CreateEmbed(msg.channel, `This Role Doesn't Have Permissions Assigned to It`, [], msg);
				
            if (typeof content == "string")
            {
                return msg.channel.send(content);
            }
            else
            {
                return msg.channel.send({ embed : content });
            }
        }

        if (args.role == "")
        {
            var perms = this.client.ListNodes();
            var categories = [];
            var nodesCategorized = {};

            perms.forEach((perm) => {
                if (nodesCategorized[perm.split(".")[0]] == null) nodesCategorized[perm.split(".")[0]] = [];
                nodesCategorized[perm.split(".")[0]].push(perm);
            });

            Object.keys(nodesCategorized).forEach((key) => {
                categories.push({ title: key.toUpperCase(), description: nodesCategorized[key] });
            });

            var fields = categories;

            var desc = `Listing All Permissions Avaliable`;
        }
        else
        {
            var perms = this.client.rolePermissions.get(args.role.id);
            var permNodes = [];

            perms.forEach((perm) => {
                permNodes.push(this.client.permissionNodes[perm]);
            });

            console.log(permNodes);
            
            var categories = [];
            var nodesCategorized = {};

            permNodes.forEach((perm) => {
                if (nodesCategorized[perm.split(".")[0]] == null) nodesCategorized[perm.split(".")[0]] = [];
                nodesCategorized[perm.split(".")[0]].push(perm);
            });

            Object.keys(nodesCategorized).forEach((key) => {
                categories.push({ title: key.toUpperCase(), description: nodesCategorized[key] });
            });

            var fields = categories;

            var desc = `Listing all permission nodes for ${args.role}`;
        }

        var content = this.client.CreateEmbed(msg.channel, desc, fields, msg);
				
        if (typeof content == "string")
        {
            return msg.channel.send(content);
        }
        else
        {
            return msg.channel.send({ embed : content });
        }
	}
};