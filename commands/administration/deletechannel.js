const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'deletechannel',
			group: 'administration',
			memberName: 'deletechannel',
			description: 'Set or get the current delete channel.',
			throttling: {
				usages: 1,
				duration: 10
			},
			args: [{ key: "channel", label: "Channel", type: "textchannel", prompt: "Select a channel to set the Deletion Channel to.", default: ''}]
		});
	}

	hasPermission(msg)	
	{
		return msg.member.hasPermission("ADMINISTRATOR") || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		if (args.channel == "")
		{
			var channel = this.client.guildData.get(msg.guild.id).deleteChannel;
			if (channel != null)
			{
				var re = new Discord.RichEmbed().setDescription(`The Current Deletion Channel is \`#${this.client.channels.get(channel).name}\``).setAuthor(msg.guild.name, msg.guild.iconURL);
				msg.channel.send({ embed : re });
			}
			else
			{
				var re = new Discord.RichEmbed().setDescription(`The Deletion Channel is Not Set`).setAuthor(msg.guild.name, msg.guild.iconURL);
				msg.channel.send({ embed : re });
			}
		}
		else
		{
			this.client.provider.db.run("UPDATE `guildSettings` SET `deleteChannel`="+args.channel.id+" WHERE `guild`="+msg.guild.id)
			this.client.guildData.get(msg.guild.id).deleteChannel = args.channel.id;
			var re = new Discord.RichEmbed().setDescription(`Set Deletion Channel to \`#${args.channel.name}\`.${this.client.guildData.get(msg.guild.id).logDeletes ? "" : " Don't Forget To Enable Message Deletion Logging For This to Take Effect!"}`).setAuthor(msg.guild.name, msg.guild.iconURL);
			msg.channel.send({ embed : re });
		}
	}
};