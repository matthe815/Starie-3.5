const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'modchannel',
			group: 'administration',
			memberName: 'channel',
			description: 'Set or get the current mod channel.',
			throttling: {
				usages: 1,
				duration: 10
			},
			args: [{ key: "channel", label: "Channel", type: "textchannel", prompt: "Select a channel to set the Mod Channel to.", default: ''}]
		});
	}

	hasPermission(msg)	
	{
		return msg.member.hasPermission("ADMINISTRATOR") || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		if (args.channel == "")
		{
			var channel = this.client.guildData.get(msg.guild.id).modChannel;
			if (channel != null)
			{
				var re = new Discord.RichEmbed().setDescription(`The Current Mod Channel is \`#${this.client.channels.get(channel).name}\``).setAuthor(msg.guild.name, msg.guild.iconURL);
				msg.channel.send({ embed : re });
			}
			else
			{
				var re = new Discord.RichEmbed().setDescription(`No Moderator Channel has Been Set`).setAuthor(msg.guild.name, msg.guild.iconURL);
				msg.channel.send({ embed : re });
			}
		}
		else
		{
			this.client.provider.db.run("UPDATE `guildSettings` SET `modChannel`="+args.channel.id+" WHERE `guild`="+msg.guild.id)
			this.client.guildData.get(msg.guild.id).modChannel = args.channel.id;
			var re = new Discord.RichEmbed().setDescription(`Moderator Channel Has Been Successfully Set to \`#${args.channel.name}\``).setAuthor(msg.guild.name, msg.guild.iconURL);
			msg.channel.send({ embed : re });
		}
	}
};