const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'logging',
			group: 'administration',
			memberName: 'logging',
			description: 'Set or get the current Logging Settings.',
			throttling: {
				usages: 3,
				duration: 10
			},
			args: [{ key: "setting", label: "Logging Setting", type: "string", prompt: "Please select a logging setting. Valid Settings: (DELETION)." }, { key: "active", label: "Activity", type: "string", prompt: "Invalid activity setting. Valid Responses: `TRUE | FALSE`.", default: '' }]
		});
	}

	hasPermission(msg)	
	{
		return msg.member.hasPermission("ADMINISTRATOR") || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		if (!msg.editable)
		{
			if (args.activity == "")
			{
				var settings = this.client.guildData.get(msg.guild.id);
				
				if (args.setting.toLowerCase() == "deletion")
				{
					return msg.reply(`Message deletion logging is: ${settings.logDeletes ? "ENABLED" : "DISABLED"}`);
				}
				else if (args.setting.toLowerCase() == "welcome")
				{
					return msg.reply(`Member welcome logged is: ${settings.welcomeChannel ? "ENABLED" : "DISABLED"}`);
				}
				else
				{
					return msg.reply(`Invalid Logging Setting, Valid logging settings are: \`DELETION\`.`);
				}
			}
			else
			{
				if (args.setting.toLowerCase() == "deletion")
				{
					var settings = this.client.guildData.get(msg.guild.id);
					var ms = await msg.channel.send("Updating value...");
					var ret = await this.client.provider.db.run(`UPDATE \`guildSettings\` SET \`logDeletes\`="${Boolean(args.active) ? 1 : 0}" WHERE \`guild\`="${msg.guild.id}"`);
					settings.logDeletes = args.active ? 1 : 0;
					return ms.edit(`Successfully set Logging Activity to \`${Boolean(args.active) ? "ENABLED" : "DISABLED"}\``);
				}
				else if (args.setting.toLowerCase() == "welcome")
				{
					var settings = this.client.guildData.get(msg.guild.id);
					var ms = await msg.channel.send("Updating value...");
					var ret = await this.client.provider.db.run(`UPDATE \`guildSettings\` SET \`welcomeMessages\`="${Boolean(args.active) ? 1 : 0}" WHERE \`guild\`="${msg.guild.id}"`);
					settings.logDeletes = args.active ? 1 : 0;
					return ms.edit(`Successfully set Logging Activity to \`${Boolean(args.active) ? "ENABLED" : "DISABLED"}\``);
				}
				else
				{
					return msg.reply(`Invalid Logging Setting, Valid logging settings are: \`DELETION\`, \`WELCOME\`.`);
				}
			}
		}
		else
		{
			if (!args.activity)
			{
				var settings = this.client.guildData.get(msg.guild.id);
			
				if (args.setting.toLowerCase() == "deletion")
				{
					return msg.edit(`Message deletion logging is: ${settings ? "ENABLED" : "DISABLED"}`);
				}
				else
				{
					return msg.edit(`Invalid Logging Setting, Valid logging settings are: \`DELETION\`.`);
				}
			}
			else
			{
				if (args.setting.toLowerCase() == "deletion")
				{
					var settings = this.client.guildData.get(msg.guild.id);
					msg.edit("Updating value...");
					var ret = await this.client.provider.db.run(`UPDATE \`guildSettings\` SET \`logDeletes\`="${args.activity ? 1 : 0}" WHERE \`guild\`="${msg.guild.id}"`);
					return msg.edit(`Successfully set Logging Activity to \`${args.activity ? "ENABLED" : "DISABLED"}\``);
				}
				else
				{
					return msg.edit(`${msg.author}, Invalid Logging Setting, Valid logging settings are: \`DELETION\`.`);
				}
			}
		}
	}
};