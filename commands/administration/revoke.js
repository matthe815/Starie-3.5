const isArray = require('util').isArray;

const discord = require('discord.js');
const Command = require('discord.js-commando').Command;

module.exports = class PruneCommmand extends Command {
	constructor(client) {
		super(client, {
			name: 'revoke',
			group: 'administration',
			memberName: 'revoke',
			aliases: ["removepermission"],
			autoAliases: true,
			description: 'Revoke a permission from a role.',
			ownerOnly: false,
			throttling: {
				usages: 2,
				duration: 5
			},

			args: [
				{
					key: 'role',
					prompt: 'Which role would you like to modify?',
					type: 'role'
				},
				{
					key: 'permission',
					prompt: 'Which permission are you trying to apply?',
                    type: 'string',
                    validate: client.ValidatePermission,
                    parse: client.GetPermission
				}
			]
		});

	}

	hasPermission(msg)
	{
		return this.client.Differentiate("ADMINISTRATOR", "MANAGE_PERMISSIONS", msg.member) || this.client.isOwner(msg.author);
	}

	async run(msg, args) {

        var addedPermissions = [];
        args.permission = args.permission.split(",");
``
        if (typeof args.permission == "string" && this.client.rolePermissions.has(args.role.id) && !this.client.rolePermissions.get(args.role.id).includes(args.permission)) {
   
            var content = this.client.CreateEmbed(msg.channel, `Role Doesn't have Permission \`${args.permission}\`!`, [], msg);
				
            if (typeof content == "string")
            {
                return msg.channel.send(content);
            }
            else
            {
                return msg.channel.send({ embed : content });
            }         
        }

        if (this.client.rolePermissions.has(args.role.id))
        {
            var permissions = this.client.rolePermissions.get(args.role.id);
            
            if (typeof args.permission == "string")
            {
                if (!permissions.includes(args.permission)) return;

                permissions.splice(permissions.indexOf(args.permission), 1);
                addedPermissions.push(args.permission);
            }
            else if (isArray(args.permission))
            {
                args.permission.forEach((perm) => {
                    if (!permissions.includes(perm)) return;
                
                    permissions.splice(permissions.indexOf(perm), 1);
                    addedPermissions.push(perm);
                });
            }

        }
        else
        {
            var permissions = [];

            if (typeof args.permission == "string")
            {
                if (!permissions.includes(args.permission)) return;

                permissions.splice(permissions.indexOf(perm), 1);
                addedPermissions.push(args.permission);
            }
            else if (isArray(args.permission))
            {
                args.permission.forEach((perm) => {
                    if (!permissions.includes(perm)) return;
                
                    permissions.splice(permissions.indexOf(perm), 1);
                    addedPermissions.push(perm);
                });
            }
        }

        this.client.SetPermissions(args.role.id, { permissions });

        var content = this.client.CreateEmbed(msg.channel, `Successfully Removed \`${addedPermissions ? addedPermissions.join(", ") : args.permission}\` to \`${args.role.name}\``, [], msg);
				
        if (typeof content == "string")
        {
            return msg.channel.send(content);
        }
        else
        {
            return msg.channel.send({ embed : content });
        }
	}
};