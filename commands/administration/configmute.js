const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class ConfigMuteCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'configmute',
			group: 'economy',
			memberName: 'configmute',
			guildOnly: false,
			ownerOnly: true,
			description: 'Configure the Mute Roling.',
		});
	}
	
	async run(msg, args) {
		var guild = this.client.guildData.get(message.guild.id);
		var mess = await msg.channel.send("Creating the `Mute` role.");
		
		// Create the role, and await it.
		message.guild.createRole({
			data: {
				name: 'Muted',
				color: 'RANDOM',
				permissions: []
			},
			reason: `Created for Muting Configuration by ${message.author.tag}!`,
		}).then(role => {
			mess.edit("Created the `Mute` role, configuring.");
			
			role.guild.channels.forEach(channel => {
            
				// Check if there's a permission overwrite for the category.
				if (channel.parent && !channel.parent.permissionOverwrites.find("id", role.id))
				{

					// If not, overwrite it. Wait 1 second between each one.
					setTimeout(() => {
						channel.parent.overwritePermissions(role, {
							SEND_MESSAGES: false,
							SPEAK: false
						});

					}, 1000);
				}
			});
			
			mess.edit("Configured successfully.");
		});
	}
};