const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'welcomemessage',
			group: 'administration',
			memberName: 'welcomemessage',
			description: 'Set or get the current Welcome Message.',
			throttling: {
				usages: 3,
				duration: 10
			},
			args: [{ key:'message', label: 'Message', type: 'string', name:'message', prompt: 'Invalid string provided.', default: ''}]
		});
	}

	hasPermission(msg)	
	{
		return msg.member.hasPermission("ADMINISTRATOR") || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		if (!msg.editable)
		{
			if (args.message == "")
			{
				var settings = this.client.guildData.get(msg.guild.id);
				
				msg.reply(`The current welcome message is: \`\`\`${settings.welcomeMessage}\`\`\`!`);
			}
			else
			{
				var settings = this.client.guildData.get(msg.guild.id);
				var ms = await msg.channel.send("Updating message...");
				var ret = await this.client.provider.db.run(`UPDATE \`guildSettings\` SET \`welcomeMessage\`="${args.message.replace(/'"'/g, "'")}" WHERE \`guild\`="${msg.guild.id}"`);
				settings.welcomeMessage = args.message;
				return ms.edit(`Successfully set Welcome Message to \`${args.message}\``);
			}
		}
		else
		{
			if (args.activity == "")
			{
				var settings = this.client.guildData.get(msg.guild.id);
				
				msg.edit(`The current welcome message is: \`\`\`${settings.welcomeMessage}\`\`\`!`);
			}
			else
			{
				var settings = this.client.guildData.get(msg.guild.id);
				var ms = await msg.edit("Updating message...");
				var ret = await this.client.provider.db.run(`UPDATE \`guildSettings\` SET \`welcomeMessage\`="${args.message.replace(/"/g, "'")}}" WHERE \`guild\`="${msg.guild.id}"`);
				settings.welcomeMessage = args.message;
				return ms.edit(`Successfully set Welcome Message to \`${args.message}\``);
			}
		}
	}
};