const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class AssignCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'assign',
			group: 'general',
			memberName: 'assign',
			ownerOnly: true,
			description: 'Self assign a role to yourself. (Incomplete)',
			throttling: {
				usages: 1,
				duration: 10
            },
            args: [
                {
                    type: 'role',
                    label: 'role',
                    key: 'role',
                    prompt: 'Please select a role to assign yourself to.'
                }
            ]
		});
	}

	async run(msg, args) {
        const assignableRoles = this.client.guildData.get(msg.guild.id).roles;

		if (!msg.editable)
		{
            const mess = await msg.reply("Assigning the role...");
            
            if (!assignableRoles.has(args.role.id)) return mess.edit("That role isn't assignable.");

            msg.member.addRole(args.role);

			return mess.edit(`The role ${args.role.name} has been assigned to you.`);		
		}
		else
		{
            await msg.edit("Assigning the role...");
            
            if (!assignableRoles.has(args.role.id)) return msg.edit("That role isn't assignable.");

            msg.member.addRole(args.role);

			return msg.edit(`The role ${args.role.name} has been assigned to you.`);		
		}
	}
};