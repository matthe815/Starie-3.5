const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'remind',
			group: 'general',
			memberName: 'remind',
			aliases: ["reminder", "remindme"],
			description: 'Create a brand new reminder',
			throttling: {
				usages: 1,
				duration: 60
			},
			args: [{ key: "time", label: "Time", type: "time", prompt: "Please select the amount of time you want to remind to go on for" }, {key: "reason", label: "reason", type: "string", prompt: "Please provide a reason for the reminder"}]
		});
	}

	async run(msg, args) {
		var time = translateTime(args.time);
		var re = new Discord.RichEmbed();
        re.setAuthor(msg.author.username, msg.author.avatarURL);
        re.setDescription("Reminder Set");
        re.addField("Remind At", new Date(time.endingAt));
        re.setTimestamp(new Date());
		
		setTimeout(() => {
            var emb = new Discord.RichEmbed();
            emb.setAuthor(msg.author.username, msg.author.avatarURL);
            emb.setDescription(`You Told Me to Remind You Of: ${args.reason}`);
            emb.setTimestamp(new Date());

            msg.channel.send(msg.author, { embed : emb });
        }, time["time"]);

        await this.client.addRecord(msg.member, msg.guild, msg.author, args.reason, "reminder");

        msg.channel.send({ embed : re });
	}
};

function translateTime(string) {
	
	var time = 0;
	
	while (string.length > 0)
	{
		var num = string.slice(0, string.search(/([a-z])/i));
		var iden = string.slice(string.search(/([a-z])/i),string.search(/([a-z])/i)+1);

		if (iden == "s")
		{
			time += 1000 * num;
			var length = num.toString().length + iden.toString().length;
			console.log(length);
			string = string.slice(length, string.length);
		}
		if (iden == "m")
		{
			time += 60000 * num;
			var length = num.toString().length + iden.toString().length;
			console.log(length);
			string = string.slice(length, string.length);
		}
		if (iden == "h")
		{
			time += 3600000 * num;
			var length = num.toString().length + iden.toString().length;
			console.log(length);
			string = string.slice(length, string.length);
		}
	}
	
	var endAt = new Date().getTime() + time;
	return { "time" : time, "endingAt" : endAt };
}