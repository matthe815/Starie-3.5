const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class CallCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'call',
			group: 'general',
			memberName: 'call',
			description: 'Calls a user.',
			throttling: {
				usages: 2,
				duration: 25
            },
            args: [
                {
                    key: "recipient",
                    type: "user",
                    prompt: "Please input a user to call.",
                    label: "Call Recipient"
                }
            ]
		});
	}

	async run(msg, args) {
		if (client.users.get(args.recipient.id)) return msg.reply("You can't call a user that I am not in a server with.");
		if (msg.editable)
		{
        	if (msg.channel.type != "dm") return msg.edit(`This command can only be used in DMs.`);
			if (args.author.bot) return msg.edit("You cannot call bots.");
			if (!this.client.userData.get(args.author.id).allowCalls) return msg.edit("This user doesn't allow calls.");

            const mess = await msg.reply("Calling user...");
           	this.client.calls.set(msg.author.id, { caller: msg.author, reciever: args.recipient });
          	this.client.calls.set(args.recipient.id, { reciever: msg.author, caller: args.recipient });
           	args.recipient.send(`${msg.author.tag} started a call with you. Say endcall to end it.`)
			return mess.edit(`${args.recipient.tag} called in ${mess.createdTimestamp - msg.createdTimestamp}ms. Say hello!`);		
		}
		else
		{
			const mess = await msg.reply("Calling user...");
			
			if (msg.channel.type != "dm") return mess.edit(`This command can only be used in DMs.`);
			if (args.author.bot) return mess.edit("You cannot call bots.");
			if (!this.client.userData.get(args.author.id).allowCalls) return mess.edit("This user doesn't allow calls.");

           	this.client.calls.set(msg.author.id, { caller: msg.author, reciever: args.recipient });
          	this.client.calls.set(args.recipient.id, { reciever: msg.author, caller: args.recipient });
           	args.recipient.send(`${msg.author.tag} started a call with you. Say endcall to end it.`)
			return mess.edit(`${args.recipient.tag} called in ${mess.createdTimestamp - msg.createdTimestamp}ms. Say hello!`);		
		}
	}
};