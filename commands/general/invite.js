const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class InviteCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'invite',
			group: 'general',
			memberName: 'invite',
			description: 'Get an invite link for a bot to the server.',
			throttling: {
				usages: 1,
				duration: 10
			}
		});
	}

	async run(msg) {
		if (!msg.editable)
		{
			const pingMsg = await msg.reply("Generating Invite...");
			return pingMsg.edit(`You can join the official server with <https://discord.gg/dwtR7pV>, \n\nYou can invite me to your server with the invite link <${await this.client.generateInvite(["ADMINISTRATOR"])}>`);		
		}
		else
		{
			await msg.edit("Generating Invite...");
			return msg.edit(`You can join the official server with https://discord.gg/dwtR7pV, \n\nYou can invite me to your server with invite me to your server with the invite link ${await this.client.generateInvite(["ADMINISTRATOR"])}!`);		
		}
	}
};