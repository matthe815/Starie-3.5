const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');

module.exports = class AfkCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'afk',
			group: 'general',
			memberName: 'afk',
			aliases: ["away"],
			description: 'Step away from the keyboard.',
			throttling: {
				usages: 1,
				duration: 15
			},
			args: [{key: "reason", label: "reason", type: "string", prompt: "Please provide a reason for being AFK", default: 'No Reason Provided'}, ]
		});
	}

	async run(msg, args) {
        var re = new Discord.RichEmbed();
        re.setDescription("Successfully Set AFK Status");
        re.setAuthor(msg.author.username, msg.author.avatarURL);
        re.setTimestamp(new Date());
        if (!this.client.userData.get(msg.author.id).afk) {
            this.client.userData.get(msg.author.id).afk = true;
            this.client.userData.get(msg.author.id).afkReason = args.reason;
            re.addField("Reason", args.reason);
            return msg.channel.send({ embed : re });
        } else {
            this.client.userData.get(msg.author.id).afk = false;
            this.client.userData.get(msg.author.id).afkReason = "";
            re.setDescription("Successfully Removed AFK Status");
            return msg.channel.send({ embed : re });
            var re = new Discord.RichEmbed();
            re.setDescription(`${msg.author.username} Is No Longer AFK.`);
            msg.channel.send({ embed : re });
        } 
    }
};