const oneLine = require('common-tags').oneLine;
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class PingCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'russian',
			group: 'general',
            memberName: 'russian',
            aliases: ["russian-roulette", "no-russian"],
			description: 'Play russian roulette!.',
			throttling: {
				usages: 10,
				duration: 10
            }
		});
	}

	async run(msg, args) {
        var chances = 6, dead = false, chosen = 0, user = args.user ? args.user : msg.member;

        chosen = Math.floor(Math.random() * chances);

        if (chosen == chances-1) dead = true;

        if (dead && user.id != "207989356059688962")
        {
            msg.reply(":fire: :gun:");

            setTimeout(() => {
                user.kick("Died in russian roulette");
            }, 10000);

            msg.channel.send(`${user}, You have 10 seconds to say goodbye to your friends.`);
        }
        else
        {
            msg.reply(":smoking: :gun:");
        }
	}
};
