const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class ConfigMuteCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'actas',
			group: 'general',
            memberName: 'actas',
            ownerOnly: true,
            description: 'Use Starie as another user (Whom has given you access to do so).',
            args: [
                {
                    key: 'user',
                    type: 'user',
                    label: 'user to access',
                    prompt: 'Please select a valid user whom has given you access to use.'
                }
            ]
		});
	}
	
	async run(msg, args) {
		
	}
};