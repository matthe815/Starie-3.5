const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');
const User = require('../../classes/userdata');
const Guild = require('../../classes/guilddata');
const fs = require('fs');

module.exports = class ConvertCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'panel',
			group: 'general',
			memberName: 'panel',
			guildOnly: false,
			ownerOnly: false,
            description: 'Enable/Disable panel access.',
            args: [{ key: 'value', label: 'Status', type: 'boolean', prompt: 'Please select `true` or `false`.'}]
		});
	}

	async run(msg, args) {
        if (!msg.editable)
        {
            var mess = await msg.reply(`Changing Panel Access to ${args.value}...`);
            await this.client.provider.db.run(`UPDATE \`userSettings\` SET \`panelAccess\`="${args.value ? 1 : 0}"`);
            this.client.userData.get(msg.author.id).panelAccess = args.value;
            mess.edit(`Successfully set Panel Access to ${args.value}`);
        }
        else
        {
            msg.edit(`Changing Panel Access to ${args.value}...`);
            await this.client.provider.db.run(`UPDATE \`userSettings\` SET \`panelAccess\`="${args.value}"`);
            this.client.userData.get(msg.author.id).panelAccess = args.value;
            msg.edit(`Successfully set Panel Access to ${args.value}`);
        }
        
	}
};