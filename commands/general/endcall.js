const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class CallCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'endcall',
			group: 'general',
			memberName: 'endcall',
			description: 'Ends a call with a user.',
			throttling: {
				usages: 2,
				duration: 25
            }
		});
	}

	async run(msg) {
        if (msg.channel.type != "dm") return msg.reply(`This command can only be used in DMs.`);
		if (this.client.calls.has(msg.author.id)) return msg.reply("You cannot end a call that doesn't exist!");
		if (!msg.editable)
		{
			var reciever = this.client.calls.get(msg.author.id).reciever;
			if (!reciever) msg.reply(`Cannot end call as the reciever is invalid.`);
            const mess = await msg.reply(`Ending call with ${reciever.tag}...`);
            reciever.send(`${msg.author.tag} has ended the call.`);
            this.client.calls.delete(reciever.id);
            this.client.calls.delete(msg.author.id);
			return mess.edit(`Call ended in ${mess.createdTimestamp - msg.createdTimestamp}ms.`);		
		}
		else
		{
			await msg.edit("Generating Invite...");
			return msg.edit(`You can join the official server with https://discord.gg/dwtR7pV, \n\nYou can invite me to your server with invite me to your server with the invite link ${await this.client.generateInvite(["ADMINISTRATOR"])}!`);		
		}
	}
};