const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class SayCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'say',
			group: 'general',
			memberName: 'say',
			guildOnly: true,
			description: 'Force Starie to say something.',
			examples: ["say Love ya!"],
			args: [{ key: "message", label: "Message", type: "string", prompt: "Please provide a message to send." }]
		});
	}
	
	async run(msg, args) {
		msg.channel.send(args.message, { disableEveryone: true });
		msg.delete();
	}
};