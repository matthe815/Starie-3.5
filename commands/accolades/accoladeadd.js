const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Accolade = require('../../classes/accolade');

module.exports = class AccoladeAddCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'accoladeadd',
			group: 'accolades',
			memberName: 'accoladeadd',
			guildOnly: true,
			ownerOnly: true,
			description: 'Add an accolade to the list.',
			examples: ["accoladeadd Die"],
			args: [{ key: "name", label: "Accolade Name", type: "string", prompt: "Please provide a name for the accolade." }]
		});
	}

	hasPermission(msg)
	{
		return this.client.isOwner(msg.author)
	}
	
	async run(msg, args) {
		var message = await msg.channel.send(`Adding Accolade ${args.name} to the list...`);
		console.log(this.client.user.username);
		var acco = new Accolade(args.name, "", "", condition(), this.client);
		this.client.addAccolade(acco).then((accolade) => {
			message.edit("Successfully added the Accolade to the list.");
		});
		console.log(acco.name);
		console.log(this.client.accolades.first().name);
	}
};

function condition()
{

}