const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Accolade = require('../../classes/accolade');

module.exports = class AccoladeAddCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'accoladeunlock',
			group: 'accolades',
			memberName: 'accoladeunlock',
			guildOnly: true,
			description: 'Unlock an accolade from the list.',
			examples: ["accoladeunlock Die"],
			args: [{ key: "accolade", label: "Accolade", type: "accolade", prompt: "Please provide an accolade to unlock." }]
		});
	}
	
	async run(msg, args) {
		args.accolade.unlock(msg.author, msg);
	}
};

function condition()
{

}