const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Accolade = require('../../classes/accolade');

module.exports = class AccoladeCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'accolades',
			group: 'accolades',
			memberName: 'accolades',
			guildOnly: false,
			description: 'Add an accolade to the list.',
			examples: ["accolades", "accolades test"],
			args: [{ key: "name", label: "Accolade's Name", type: "string", prompt: "Please provide the name of the accolade", default: '' }]
		});
	}
	
	async run(msg, args) {
		if (args.name == "")
		{
			var list = "__Global Accolades:__\n";
			var message = await msg.channel.send(`Fetching accolade list...`);
			this.client.accolades.forEach((accolade) => { 
				list = list + accolade.name + "\n";
			});
			
			list = list + "\n\nType `accolades accolade` to get detailed information on the accolade.";
			
			message.edit("Successfully fetched the Accolade List, and DMed them to you.");
			msg.author.send(list);
		}
		else
		{
			var accolade = this.client.accolades.find("name", args.name);
			msg.author.send(`--------------__Detailed Accolade View__--------------\n\n**Icon:** ${accolade.icon},\n**Name:** ${accolade.name},\n**Description:** ${accolade.description},\n**How to Get:** ${accolade.conditions}`);
		}
	}
};