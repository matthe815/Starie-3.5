const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const ytdl = require('ytdl-core');
const streamOptions = {};
const Discord = require("discord.js");

module.exports = class PlayCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'play',
			group: 'music',
			memberName: 'play',
			guildOnly: true,
			ownerOnly: true,
			description: 'Request music to be played.',
			args: [{ key: "song", label: "Song", type: "string", prompt: "Please provide a song to play." }]
		});
	}

	async run(msg, args) {
		if (!msg.editable) {
			const re = new Discord.RichEmbed();
			re.setAuthor(msg.guild.name, msg.guild.iconURL);
			msg.member.voiceChannel.join().then(async (conn) => {
					console.log(`Playing song: ${args.song}`);
					console.log(args.song);
					if (!ytdl.validateURL(args.song)) { re.setDescription("Invalid URL"); return msg.channel.send({ embed : re }); }
					const info = await ytdl.getInfo(args.song);
					console.log(info);
					const stream = ytdl(args.song, {filter:'audioonly',
						requestOptions: {
						  transform: (parsed) => {
							return {
							  host: '127.0.0.1',
							  port: 8888,
							  path: '/' + parsed.href,
							  headers: { Host: parsed.host },
							};
						  },
						}
					  });
					const broadcast = this.client
					.createVoiceBroadcast()
					.playStream(stream);
					const dispatcher = conn.playBroadcast(broadcast, streamOptions);
					re.setDescription(`Now Playing: ${args.song}`);
					msg.channel.send({ embed : re });
		
					dispatcher.on('end', (reason) => {
						console.log(reason);
						
						if (queue[0])
						{
							dispatcher.stream.playStream(ytdl(queue[0]["url"], { filter : 'audioonly' }));
						}
						else
						{
							msg.channel.send(`No more songs queued, please queue some more songs!`);
							msg.guild.me.voiceChannel.leave();
						}
					});
			});
		}
		else
		{
			await msg.edit("Adding music to the request list...");
			message.member.voiceChannel.join().then(conn => {
				if (!conn.speaking)
				{
					console.log(`Playing song: ${args.song}`);
					const stream = ytdl(args.song, { filter : 'audioonly' });
					const broadcast = this.client
					.createVoiceBroadcast()
					.playStream(stream);
					const dispatcher = conn.playBroadcast(broadcast, streamOptions);
		
					dispatcher.on('end', (reason) => {
						console.log(reason);
						
						if (queue[0])
						{
							dispatcher.stream.playStream(ytdl(queue[0]["url"], { filter : 'audioonly' }));
						}
						else
						{
							msg.channel.send(`No more songs queued, please queue some more songs!`);
						}
					});
				}
				else
				{
					// Add the item to the array.
					queue.push(String(args.song));
			
					msg.edit(`Successfully added the song: ${args[0]}`);
				}
			});
		}
	}
};