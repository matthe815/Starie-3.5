const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');
var Battle = require("../../classes/battle");

module.exports = class CharacterCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'battle',
			group: 'rpg',
            memberName: 'battle',
            ownerOnly: true,
            guildOnly: true,
			aliases: ["fight","fuckup","rob","beatup"],
			description: 'Challenge another player to a battle.',
			throttling: {
				usages: 1,
				duration: 15
            },
            args: [{ key: "user", label: "Opponent", type: "user", prompt: "Please enter the user's name"}]
		});
	}
	
	async run(msg, args) {
        var opp = args.user;
		var battle = await this.client.addBattle(opp, msg);
		
		var emb = new Discord.RichEmbed();
		emb.setAuthor(msg.author.username, msg.author.avatarURL);
		emb.setDescription(`A Battle Has Been Created Against ${opp.username}`);
		emb.setURL(`https://starie.ga/battle?id=${battle.id}`);

		msg.channel.send({ embed : emb });
	}
};