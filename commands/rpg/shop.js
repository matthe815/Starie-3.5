const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');

module.exports = class ShopCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'shop',
			group: 'rpg',
			memberName: 'shop',
			aliases: ["s","buy"],
			description: 'Buy some new gear for your RPG character.',
			throttling: {
				usages: 2,
				duration: 30
            },
            args: [{ key: "item", label: "Item Name", type: "string", prompt: "Please enter the item's name.", default: ''}]
		});
	}
	
	async run(msg, args) {
        var user = this.client.userData.get(msg.author.id);
        var guild = this.client.guildData.get(msg.guild.id);
        let character = user.character;
        if (!guild.shop || guild.shop.outOfTime()) guild.newShop();

        if (args.item == '') {
            var mess = await msg.reply("Loading the shop...");
            if (!guild.shop) guild.newShop();
            let emb = new Discord.RichEmbed();
            emb.setAuthor(msg.guild.name, msg.guild.avatarURL);

            if (character)
            {
                guild.shop.items.forEach((item) => {
                    emb.addField(`${item.name} - $${item.price}`, `Might: ${item.might}\nLevel: ${item.level}\nRelative Strength: ${item.might - getEquippedMight(item.type, character)}`);
                });
            }
            else
            {
                guild.shop.items.forEach((item) => {
                    emb.addField(`${item.name} - $${item.price}`, `Might: ${item.might}\nLevel: ${item.level}`);
                });
            }
            emb.setTimestamp(new Date());
            mess.edit(emb);
        }
        else
        {
            if (!character) return msg.reply("You must have a character before purchasing an item.");
            var mess = await msg.reply("Purchasing item...");
            let item = validate(guild.shop.items, args.item);
            if (typeof item == 'string') return mess.edit(item);
            if (!item) return mess.edit("Couldn't find the item that you requested.");
            if (user.money < item.price) return mess.edit("You cannot afford this item.");
            if (character.equipped(item.type)) character.unequip(item.type);
            character.equip(item.type, item);
            user.money -= item.price;
            this.client.provider.db.run(`UPDATE \`userSettings\` SET \`money\`="${user.money}" WHERE \`id\`="${user.user}"`);
            mess.edit(`Successfully purchased ${item.name}!`);
        }
	}
};

const disambiguation = require('discord.js-commando').util.disambiguation;
const escapeMarkdown = require('discord.js').escapeMarkdown;

function validate(itemList, value) {
	const search = value.toLowerCase();
	let items = itemList.filter(itemFilterInexact(search));
	if(items.length === 0) return false;
	if(items.length === 1) return items[0];
	const exactItems = items.filter(itemFilterExact(search));
	if(exactItems.length === 1) return items[0];
	if(exactItems.length > 0) items = exactItems;
	return items.length <= 15 ?
		`${disambiguation(
			items.map(item => `${escapeMarkdown(item.name)}`), 'items', null
		)}\n` :
		'Multiple items found. Please be more specific.';
}


function itemFilterExact(search) {
	return item => item.name.toLowerCase() === search;
}

function itemFilterInexact(search) {
	return item => item.name.toLowerCase().includes(search);
}

function getEquippedMight(type, character) {
    let equip = JSON.parse(character.stringifyEquipment());
    if (equip[type] == undefined) return 0;
    return equip[type]["might"];
}