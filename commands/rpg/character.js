const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');

module.exports = class CharacterCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'character',
			group: 'rpg',
			memberName: 'character',
			aliases: ["char","stats","rpgprofile","rpg"],
			description: 'Get the profile for a character.',
			throttling: {
				usages: 2,
				duration: 15
            },
            args: [{ key: "user", label: "Profile Name", type: "user", prompt: "Please enter the user's name.", default: ''}]
		});
	}
	
	async run(msg, args) {
        if (!msg.editable)
        {
            if (args.user == '') args.user = msg.author;
            var user = this.client.userData.get(args.user.id);
            let character = user.character;

            var re = new Discord.RichEmbed();
            re.setAuthor(msg.guild.name, msg.guild.iconURL);
            re.setDescription(`You cannot look up a character that doesn't exist.`);
            
            if (!character) return msg.channel.send({ embed : re });

            let emb = new Discord.RichEmbed();
            emb.setAuthor(args.user.tag, args.user.displayAvatarURL);
            emb.addField("Name", character.name);
            emb.addField("Might", character.might);
            emb.addField("Equipment", character.getEquipment().length != 0 ? character.getEquipment() : "Naked");
            emb.setFooter(character.getMight());
            emb.setTimestamp(new Date());
            msg.channel.send({ embed : emb });
        }
        else
        {
            if (args.user == '') args.user = msg.author;
            var user = this.client.userData.get(args.user.id);
            let character = user.character;

            var re = new Discord.RichEmbed();
            re.setAuthor(msg.guild.name, msg.guild.iconURL);
            re.setDescription(`You cannot look up a character that doesn't exist.`);
            
            if (!character) return msg.channel.send({ embed : re });

            let emb = new Discord.RichEmbed();
            emb.setAuthor(args.user.tag, args.user.displayAvatarURL);
            emb.addField("Name", character.name);
            emb.addField("Might", character.might);
            emb.addField("Equipment", character.getEquipment().length != 0 ? character.getEquipment() : "Naked");
            emb.setFooter(character.getMight());
            emb.setTimestamp(new Date());
            msg.channel.send({ embed : emb });
        }
	}
};