const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'create',
			group: 'rpg',
			memberName: 'create',
			aliases: ["c","createcharacter","newrpg"],
			description: 'Create a brand new RPG Character',
			throttling: {
				usages: 1,
				duration: 30
			},
			args: [{ key: "name", label: "Name", type: "string", prompt: "Select a name for your character"}, { key: "gender", label: "gender", type: "boolean", prompt: "Please select your character's gender" }]
		});
	}
	
	async run(msg, args) {
        var mess = await msg.reply("Awesome! Creating your brand new character!");
        var user = this.client.userData.get(msg.author.id);
        var guild = this.client.guildData.get(msg.guild.id);
        let character = user.character;

        if (character) return mess.edit(`Unfortunately, you already have a character... If you want to reset ${user.character.gender ? "him" : "her"}, please use the \`reset\` command.`);
        let char = await user.createCharacter(args.name, args.gender);

        if (!char) return mess.edit("We couldn't create your character for some reason.");
        if (!guild.shop) guild.newShop();
        mess.edit("Your new character has been created! You've been given $50 to spend on equipment! Feel free to spend it via the online store, or with the `shop` command.")
	}
};