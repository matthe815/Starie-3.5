const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');
const User = require('../../classes/userdata');
const Guild = require('../../classes/guilddata');
const fs = require('fs');

module.exports = class ConvertCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'convert',
			group: 'util',
			memberName: 'convert',
			guildOnly: false,
			ownerOnly: true,
			description: 'Converts all User & Guild data files into Rows.',
		});
	}
    
    hasPermission(msg)
    {
        return this.client.isOwner(msg.author);
    }

	async run(msg) {
        var mess = await msg.reply("Converting...");

        var userDatabase = fs.readdirSync("./database/Users");
        var guildDatabase = fs.readdirSync("./database/Servers");

        mess.edit(`${userDatabase.length + guildDatabase.length} users and guilds are being converted...`);
        
        guildDatabase.forEach((guild) => {
            var json = fs.readFileSync(`./database/Servers/${guild}`);
            var info = JSON.parse(json);
            info["id"] = guild.replace(".json", "");
            if (!info["channels"]) info["channels"] = {};
            if (!info["muting"]) info["muting"] = {};
            var guild = new Guild(this.client.guildData.size+1, guild.replace(".json", ""), info["channels"]["mod_channel"] ? info["channels"]["mod_channel"] : null, 0, null, null, info["muting"]["muted_role"] ? info["muting"]["muted_role"] : null, 500, { "administrator" : ["*"], "moderator" : ["VIEW_LOGS", "VIEW_PANEL", "DELETE_LOGS", "PIN_LOGS" ] }, this.client);
            this.client.addGuildFromFile(guild);
        });

        mess.edit(`${userDatabase.length} objects remaining...`);

        userDatabase.forEach((user) => {
            var json = fs.readFileSync(`./database/Users/${user}`);
            var info = JSON.parse(json);
            info["id"] = user.replace(".json", "");
            var user = new User(this.client.userData.size+1, user.replace(".json", ""), null, info["panelAccess"] ? info["panelAccess"] : 0, info["money"] ? info["money"] : 0, 0, this.client);
            this.client.addUserFromFile(user);
        });

        mess.edit(`Finished processing ${userDatabase.length - guildDatabase.length} objects.`);
        
	}
};