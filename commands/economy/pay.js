const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class PayCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'pay',
			group: 'economy',
			memberName: 'pay',
			guildOnly: false,
			description: 'Pay a user some money.',
			args: [
				{
					key: "amount",
					label: "Amount",
					type: "integer",
					prompt: "The amount of money to send"
				},
				{
					key: "reciever",
					label: "Recipient",
					type: "user",
					prompt: "The user to send the money to."
				}
			]
		});
	}
	
	async run(msg, args) {
			var user = this.client.userData.get(msg.author.id);	
			var user2 = this.client.userData.get(args.reciever.id);
			
			var re = new Discord.RichEmbed();
			re.setAuthor(msg.guild.name, msg.guild.iconURL);
			re.setDescription("You Cannot Pay Money to a Bot");

			if (args.reciever.bot) return msg.channel.send({ embed : re });

			if (!user2)
			{
				this.client.addUser(args.reciever);
				user2 = this.client.userData.get(args.reciever);
			}

			var total = user.money - args.amount;
			var total2 = user2.money + args.amount;
		
			var re = new Discord.RichEmbed();
			re.setAuthor(msg.guild.name, msg.guild.iconURL);
			re.setDescription("You Must Send More Than $0");

			if (args.amount <= 0) return msg.channel.send({ embed : re });
			if (user.money >= args.amount) {
				this.client.provider.db.run(`UPDATE userSettings SET \`money\`="${total2}" WHERE \`user\`="${args.reciever.id}";`);
				this.client.provider.db.run(`UPDATE userSettings SET \`money\`="${total}" WHERE \`user\`="${msg.author.id}";`);
				user.money -= args.amount;
				user2.money += args.amount;
				var re = new Discord.RichEmbed();
				re.setAuthor(msg.guild.name, msg.guild.iconURL);
				re.setDescription(`Successfully sent $${args.amount} to \`${args.reciever.tag}\``);
				return msg.channel.send({ embed : re });
			}
			else
			{
				var re = new Discord.RichEmbed();
				re.setAuthor(msg.guild.name, msg.guild.iconURL);
				re.setDescription("You Don't Have Enough Money to Send");
				return msg.channel.send({ embed : re });
			}
	}
};