const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class DailyCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'daily',
			group: 'economy',
			memberName: 'daily',
			guildOnly: false,
			description: 'Obtain your daily currency.',
			args: [
				{
					key: 'reciever',
					label: 'Reciever',
					prompt: 'Choose a reciever',
					type: 'member',
					default: ''
				}
			]
		});
	}
	
	async run(msg, args) {
			var user = this.client.userData.get(args.reciever ? args.reciever.id : msg.author.id);
			
			var content = this.client.CreateEmbed(msg.channel, "You Cannot Send Your Daily Check to a Bot!" , [], msg, null, false)
			if (args.reciever.bot) { if (typeof content == "string") { return msg.channel.send(content); } else { return msg.channel.send({ embed : content }); } }

			if (!user)
			{
				this.client.addUser(args.reciever);
				user = this.client.userData.get(args.reciever);
			}

			var sender = this.client.userData.get(msg.author.id);
		
			if (new Date().getTime() - sender.dailyTime < 86400000)
			{
				var ddate = new Date(sender.dailyTime);
				var time = new Date(sender.dailyTime + 86400000);

				var content = this.client.CreateEmbed(msg.channel, "You've Already Cashed Your Daily Check!", [], msg, null, false);
				if (typeof content == "string") { return msg.channel.send(content); } else { return msg.channel.send({ embed : content }); }
			}
			else
			{
			user.money += 100 + Math.floor(Math.random() * 100);
			sender.dailyTime = new Date().getTime();
			var query = "UPDATE `userSettings` SET `money`="+user.money+" WHERE _rowid_='"+user.id+"';";
			var query2 = "UPDATE `userSettings` SET `dailyTime`="+sender.dailyTime+" WHERE _rowid_='"+msg.author.id+"';";
			this.client.provider.db.run(query);
			this.client.provider.db.run(query2);
				
			var content = this.client.CreateEmbed(msg.channel, `💳 ${args.reciever ? "Successfully Sent Your Daily Check to " + args.reciever.user.tag + "!" : "Successfully Cashed Your Daily Check."}`, [], msg, null, false);

			if (typeof content == "string") { return msg.channel.send(content); } else { return msg.channel.send({ embed : content }); }
		}
	}
};

function condition()
{

}