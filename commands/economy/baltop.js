const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class BaltopCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'baltop',
			aliases: ["top", "balancetop", "leaderboard"],
			group: 'economy',
			memberName: 'baltop',
			guildOnly: false,
			description: 'List the top 10 money holders.',
			throttling: {
				usages: 1,
				duration: 5
			}
		});
	}
	
	async run(msg, args) {
		var top = await this.client.balanceTop(10);
		var leaderboard = [];
		for (const row of top)
		{
			let user;
			try {
				console.log(`${this.client.users.get(row.user).tag}: ${row.money}`);
				leaderboard.push(`${this.client.users.get(row.user).tag}: $${row.money}`);
			} catch (e) {
				this.client.emit("warn", `Failed to load data for user ${row.user}`)
				continue;
			}
		}
		
		this.client.emit("leaderboardFetched", (leaderboard));
		
		var emb = new Discord.RichEmbed();
		emb.setAuthor("Balance Leaderboard");
		var list = "";
		
		leaderboard.forEach((item) => {
			list = `${list}**${item}**\n`;
		});
		
		emb.setDescription(list);
		
		return msg.channel.send({ embed : emb });
	}
};

function condition()
{

}