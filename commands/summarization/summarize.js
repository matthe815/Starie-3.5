const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');
const Request = require('request');
const rp = require('request-promise');

module.exports = class SummarizeCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'summarize',
			group: 'summarization',
			memberName: 'summarize',
			guildOnly: false,
			description: 'Get a summarization of a link.',
			throttling: {
				usages: 1,
				duration: 20
			},
			args: [
				{
					key: 'site',
					label: 'website',
					type: 'string',
					prompt: 'Please enter a website for me to summarize.'
				}
			]
		});
	}
	
	async run(msg, args) {
		if (!msg.editable)
		{
			var mess = await msg.reply(`Crawling website...`);
			var crawled = await rp(args.site)
			mess.edit(`Parsing website...`);
			var body1 = crawled.indexOf(">", crawled.search("<body"))+1;
			var body2 = crawled.indexOf(">", crawled.search("</body"))+1;
			var body = crawled.slice(body1+1, body2-7);
			var nav1 = body.search("<nav");
			var nav2 = body.search("</nav>");
			var nav = body.slice(nav1, nav2+7);
			body = body.replace(nav, "");
			var footer1 = body.search("<footer>");
			var footer2 = body.search("</footer>");
			var footer = body.slice(footer1, footer2);
			body = body.replace(footer, "");
			console.log(body);
			let i = 0;
			let lb=0, rb=0;
			while (i<body.length) {
				lb = body.indexOf("<", i-1, 1);
				rb = body.indexOf(">", i-1, 1)+1;
				if (lb > 0 && rb > 0) console.log(`${lb} | ${rb} | ${body.slice(lb,rb)}`);
				if (lb != 0 && rb != 0) { let sliced = body.slice(lb,rb); body = body.replace(sliced, ""); lb=0;rb=0;}
				i++;
			}
			
			var halfSlice1 = "", halfSlice2 = "";
			if (body.length > 2000) halfSlice1 = body.slice(0,1999); halfSlice2 = body.slice(2000, body.length);
			
			if (halfSlice1 == "") return mess.edit(body);
			mess.edit(halfSlice1);
			return mess.channel.send(halfSlice2);
		}
		else
		{
		
		}
	}
};