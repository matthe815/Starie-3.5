const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;

module.exports = class BanCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'unban',
			group: 'moderation',
			memberName: 'unban',
			guildOnly: true,
			guarded: true,
			userPermissions: ["MANAGE_MESSAGES"],
			clientPermissions: ["BAN_MEMBERS"],
			description: 'Unban a user from the guild.',
			examples: ["unban Matthe815", "unban Matthe815 Because Reasons", "unban 207989356059688962"],
			args: [{ key: "user", label: "Member Resolvable", type: "user", prompt: "Please provide a user to unban." }, { key: "reason", label: "Reason", default: "No Reason Provided", type: "string", prompt: "Please provide a reason for kicking the ban." }]
		});
	}

	hasPermission(msg)
	{
		return msg.member.hasPermission("MANAGE_MESSAGES") || this.client.isOwner(msg.author)
	}
	
	async run(msg, args) {
		if (msg.editable)
		{
            var user = args.user;

			msg.edit("Unbanning...");

			if (msg.member.hasPermission("ADMINISTRATOR"))
			{
				try {
					await msg.guild.unban(user.id, args.reason);
						msg.edit(`Successfully unbanned @${args.user.tag}.`);
					} catch(e) {
						return msg.edit(`Failed to unban @${args.user.tag}, \`${e}\`.`);
				}
			}
		}
		else
		{
            var user = args.user;

			var mess = await msg.reply("Unbanning...");

			if (msg.member.hasPermission("ADMINISTRATOR"))
			{
				try {
					await msg.guild.unban(user.id, args.reason);
						mess.edit(`Successfully unbanned @${args.user.tag}.`);
					} catch(e) {
						return mess.edit(`Failed to unban @${args.user.tag}, \`${e}\`.`);
				}
			}
        }
    }
};