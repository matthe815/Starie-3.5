const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class KickCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'warn',
			group: 'moderation',
			memberName: 'warn',
			guildOnly: true,
			guarded: true,
			userPermissions: ["MANAGE_MESSAGES"],
			description: 'Warn a user.',
			args: [{ key: "user", label: "Member Resolvable", type: "member", prompt: "Please provide a user to warn" }, { key: "reason", label: "Reason", default: "No Reason Provided", type: "string", prompt: "Please provide a reason for warning the user" }]
		});
	}

	hasPermission(msg)
	{
		return msg.member.hasPermission("MANAGE_MESSAGES") || this.client.isOwner(msg.author)
	}
	
	async run(msg, args) {
		if (!args.user.hasPermission("ADMINISTRATOR") || this.client.isOwner(msg.author))
		{
			try {
				const re = new Discord.RichEmbed();
				re.setDescription(`Successfully Warned \`@${args.user.user.tag}\``);
				re.setFooter(`Case: #${this.client.records++}`);
                re.setAuthor(msg.author.username, msg.author.avatarURL);
                re.setTimestamp(new Date());
                this.client.addRecord(args.user, msg.guild, msg.author, args.reason, "warn");
                
                var warns = await this.client.checkWarns(args.user, msg.guild);
                
                if (warns >= 5)
                {
                    if (!msg.guild.me.hasPermission("KICK_MEMBERS") && args.user.kickable)
                    {
                        const re = new Discord.RichEmbed();
                        re.setDescription(`${args.user.user.tag} Met The Warn Threshold But I Can't Kick Anyone`);
                        re.setTitle("Error");
                        re.setColor("#FF0000");
                        return msg.channel.send({ embed : re });
                    }
                    else if (!args.user.kickable)
                    {
                        const re = new Discord.RichEmbed();
                        re.setDescription(`${args.user.user.tag} Met The Warn Threshold But I Can't Kick Them`);
                        re.setTitle("Error");
                        re.setColor("#FF0000");
                        return msg.channel.send({ embed : re });                        
                    }
                    else
                    {
                        await args.user.kick("Too Many Warns");
                        
                        this.client.addRecord(args.user, msg.guild, msg.author, args.reason, "kick");
                    }
                }
					
				msg.channel.send({ embed : re });
			} catch(e) {
				const re = new Discord.RichEmbed();
				re.setDescription(e);
				re.setTitle("Error");
				re.setColor("#FF0000");
				return msg.channel.send({ embed : re });
			}
		} else {
            if (args.user.hasPermission("ADMINISTRATOR"))
            {
                const re = new Discord.RichEmbed();
                re.setDescription(`You Can't Warn Administrators`);
                re.setTitle("Error");
                re.setColor("#FF0000");
                return msg.channel.send({ embed : re });
            }

			const re = new Discord.RichEmbed();
			re.setDescription(`You Must Have The \`Administrator\` Permission To Warn Moderators`);
			re.setTitle("Error");
			re.setColor("#FF0000");
			return msg.channel.send({ embed : re });
		}
	}
};