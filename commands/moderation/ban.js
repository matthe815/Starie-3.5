const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class BanCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'ban',
			group: 'moderation',
			memberName: 'ban',
			guildOnly: true,
			guarded: true,
			userPermissions: ["MANAGE_MESSAGES"],
			clientPermissions: ["BAN_MEMBERS"],
			description: 'Ban a user from the guild.',
			examples: ["ban @Matthe815", "ban @Matthe815 Because Reasons"],
			args: [{ key: "user", label: "Member Resolvable", type: "user", prompt: "Please provide a user to ban." }, { key: "reason", label: "Reason", default: "No Reason Provided", type: "string", prompt: "Please provide a reason for kicking the ban." }]
		});
	}

	hasPermission(msg)
	{
		return this.client.Differentiate("MANAGE_MESSAGES", "BAN_MEMBERS", msg.member) || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		if (!msg.guild.members.has(args.user)) { 
			await msg.guild.ban(args.user.id, args.reason);

			const re = new Discord.RichEmbed();
			re.setDescription(`Successfully Banned \`@${args.user.tag}\``);
			re.addField("Case ID", this.client.records++);
			re.setAuthor(msg.author.username, msg.author.avatarURL);
				
			this.client.addRecord(args.user, msg.guild, msg.author, args.reason, "ban");
				
			return msg.channel.send({ embed : re });
		}

		args.user = msg.guild.members.get(args.user.id);

		if (!args.user.hasPermission("MANAGE_MESSAGES") || msg.member.hasPermission("ADMINISTRATOR"))
		{
			if (args.user.bannable)
			{
				if (!args.user.hasPermission("ADMINISTRATOR") || msg.guild.ownerId == msg.member.id)
				{
					try {
						await args.user.ban(args.reason);

						const re = new Discord.RichEmbed();
						re.setDescription(`Successfully Banned \`@${args.user.user.tag}\``);
						re.addField("Case ID", this.client.records++);
						re.setAuthor(msg.author.username, msg.author.avatarURL);
							
						this.client.addRecord(args.user, msg.guild, msg.author, args.reason, "ban");
							
						msg.channel.send({ embed : re });
					} catch(e) {
						return msg.edit(`Failed to ban @${args.user.user.tag}, \`${e}\`.`);
					}
				}
				else
				{
					const re = new Discord.RichEmbed();
					re.setDescription(`Only The Owner Can Ban Administrators`);
					re.setTitle("Error");
					re.setColor("#FF0000");
					return msg.channel.send({ embed : re });
				}
			}
			else
			{
				const re = new Discord.RichEmbed();
				re.setDescription(`I Don't Have Permission To Ban ${args.user.user.tag}`);
				re.setTitle("Error");
				re.setColor("#FF0000");
				return msg.channel.send({ embed : re });
			}
		} else {
			const re = new Discord.RichEmbed();
			re.setDescription(`You Must Have The \`Administrator\` Permission To Ban Moderators`);
			re.setTitle("Error");
			re.setColor("#FF0000");
			return msg.channel.send({ embed : re });
		}
	}
};