const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class KickCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'clear',
			group: 'moderation',
			memberName: 'clear',
			guildOnly: true,
			guarded: true,
			userPermissions: ["MANAGE_MESSAGES"],
			description: 'Clears a user\'s record.',
			args: [{ key: "user", label: "Member Resolvable", type: "member", prompt: "Please provide a user to warn" }, { key: "reason", label: "Reason", default: "No Reason Provided", type: "string", prompt: "Please provide a reason for warning the user" }]
		});
	}

	hasPermission(msg)
	{
		return msg.member.hasPermission("MANAGE_MESSAGES") || this.client.isOwner(msg.author)
	}
	
	async run(msg, args) {
        await this.client.clearUser(args.user, msg.guild);
        
        this.client.addRecord(args.user, msg.guild, msg.author, args.reason, "clear");
        
        const re = new Discord.RichEmbed();
        re.setDescription(`Successfully Cleared \`@${args.user.user.tag}\`'s Record`);
        re.setAuthor(args.user.user.tag, args.user.user.avatarURL);
        return msg.channel.send({ embed : re });
	}
};