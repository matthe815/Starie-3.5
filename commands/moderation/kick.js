const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class KickCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'kick',
			group: 'moderation',
			memberName: 'kick',
			guildOnly: true,
			guarded: true,
			userPermissions: ["MANAGE_MESSAGES"],
			clientPermissions: ["KICK_MEMBERS"],
			description: 'Kick a user from the guild.',
			args: [{ key: "user", label: "Member Resolvable", type: "member", prompt: "Please provide a user to kick." }, { key: "reason", label: "Reason", default: "No Reason Provided", type: "string", prompt: "Please provide a reason for kicking the user." }]
		});
	}

	hasPermission(msg)
	{
		return this.client.Differentiate("MANAGE_MESSAGES", "KICK_MEMBERS", msg.member) || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		if (!args.user.hasPermission("MANAGE_MESSAGES") || msg.member.hasPermission("ADMINISTRATOR"))
		{
			if (args.user.kickable)
			{
				try {
					await args.user.kick(args.reason);

					const re = new Discord.RichEmbed();
					re.setDescription(`Successfully Kicked \`@${args.user.user.tag}\``);
					re.addField("Case ID", this.client.records++);
					re.setAuthor(msg.author.username, msg.author.avatarURL);
					
					this.client.addRecord(args.user, msg.guild, msg.author, args.reason, "kick");
					
					msg.channel.send({ embed : re });
				} catch(e) {
					const re = new Discord.RichEmbed();
					re.setDescription(e);
					re.setTitle("Error");
					re.setColor("#FF0000");
					return msg.channel.send({ embed : re });
				}
			}
			else
			{
				const re = new Discord.RichEmbed();
				re.setDescription(`I Don't Have Permission To Kick ${args.user.user.tag}`);
				re.setTitle("Error");
				re.setColor("#FF0000");
				return msg.channel.send({ embed : re });
			}
		} else {
			const re = new Discord.RichEmbed();
			re.setDescription(`You Must Have The \`Administrator\` Permission To Kick Moderators`);
			re.setTitle("Error");
			re.setColor("#FF0000");
			return msg.channel.send({ embed : re });
		}
	}
};