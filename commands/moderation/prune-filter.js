const discord = require('discord.js');
const Command = require('discord.js-commando').Command;

module.exports = class PruneCommmand extends Command {
	constructor(client) {
		super(client, {
			name: 'prune-filter',
			group: 'moderation',
			memberName: 'prune-filter',
			aliases: ["bulk-filter","filter"],
			clientPermissions: ["MANAGE_MESSAGES"],
			autoAliases: true,
			description: 'Delete a large sum of messages with a filter',
			ownerOnly: false,
			throttling: {
				usages: 2,
				duration: 30
			},

			args: [
				{
					key: 'number',
					prompt: 'What number of messages do you want to prune?',
					type: 'integer',
					min: 1,
					max: 100
				},
				{
					key: 'reason',
					prompt: 'What reason is this being done for?',
					type: 'string',
					default: 'No Reason Provided'
				},
				{
					key: 'type',
					prompt: 'What type of filter would you like to apply?',
					type: 'string',
					oneOf: ["", "images", "links"]
				},
				{
					key: 'sectype',
					prompt: 'Please enter a secondary type',
                    type: 'string',
                    default: [],
					infinite: true
				}
			]
		});

	}

	hasPermission(msg)
	{
		return msg.member.hasPermission("MANAGE_MESSAGES") || this.client.isOwner(msg.author);
	}

	async run(msg, args) {
		var checks = args.number;
		var runTimes = 1;

		if (args.number > 1)
		{
			if (args.type != "")
			{
				var fmessages = new discord.Collection();

				fmessages = fmessages.concat(await msg.channel.fetchMessages({ limit: 100 }));
				fmessages = fmessages.concat(await msg.channel.fetchMessages({ limit: 100, before: fmessages.last().id }));

				if (args.type == "images")
				{
					fmessages = fmessages.filter((msg) => { return msg.attachments.size > 0; });
				}
				else if (args.type == "links")
				{
					fmessages = fmessages.filter((msg) => { return CheckForLink(msg.content); });
				}
				else if (args.type == "user" && args.sectype != null)
				{
					var users = args.sectype;

					users.forEach((user) => {
						users[users.indexOf(user)] = this.client.registry.types.get("member").parse(user, msg, users);
					});

					var size = 0;
					fmessages = fmessages.filter((msg) => { if (users.includes(msg.member) && size < args.number) { size++; return true; } else return false; });
				}
			}

			while (runTimes > 0)
			{
				await this.client.addRecord(msg.author, msg.guild, msg.author, args.reason, "Prune");

				var ntd = checks > 100 ? 100 : checks;

				var messages = await msg.channel.bulkDelete(args.type ? fmessages.array() : ntd, true);
				var content = this.client.CreateEmbed(msg.channel, `Successfully Pruned ${messages.size} Messages`, [{title: "Requested Quantity", description: args.number},{title:"Deleted Quantity", description: messages.size-1}], msg);
				
				if (typeof content == "string")
				{
					return msg.channel.send(content);
				}
				else
				{
					return msg.channel.send({ embed : content });
				}
				
				runTimes--;
			}
		}
		else
		{
			this.client.addRecord(msg.author, msg.guild, msg.author, args.reason, "Prune");

			var messages = await msg.channel.bulkDelete(2);
			var content = this.client.CreateEmbed(msg.channel, `Successfully Pruned 1 Message`, [{title: "Requested Quantity", content: args.number},{title:"Deleted Quantity", content: messages.size-1}], msg);
			
			if (typeof content == "string")
			{
				return msg.channel.send(content);
			}
			else
			{
				return msg.channel.send({ embed : content });
			}
		}
	}
};

function CheckForLink(content) {
	if (/^(http|https)/.test(content)) return true;

    return false;
}