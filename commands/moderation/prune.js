const discord = require('discord.js');
const Command = require('discord.js-commando').Command;

module.exports = class PruneCommmand extends Command {
	constructor(client) {
		super(client, {
			name: 'prune',
			group: 'moderation',
			memberName: 'prune',
			aliases: ["bulkdelete","delete"],
			clientPermissions: ["MANAGE_MESSAGES"],
			autoAliases: true,
			description: 'Delete a large sum of messages.',
			ownerOnly: false,
			throttling: {
				usages: 3,
				duration: 30
			},

			args: [
				{
					key: 'number',
					prompt: 'What number of messages do you want to prune?',
					type: 'integer',
					min: 1,
					max: 300
				},
				{
					key: 'reason',
					prompt: 'What reason is this being done for?',
					type: 'string',
					default: 'No Reason Provided'
				}
			]
		});

	}

	hasPermission(msg)
	{
		return this.client.Differentiate("MANAGE_MESSAGES", "PRUNE_MESSAGES", msg.member) || this.client.isOwner(msg.author);
	}

	async run(msg, args) {
		var checks = args.number;
		var runTimes = 1;

		while (checks > 100)
		{
			checks-=100;
			runTimes++;
		}

		if (args.number > 1)
		{
			while (runTimes > 0)
			{
				await this.client.addRecord(msg.author, msg.guild, msg.author, args.reason, "Prune");

				var ntd = checks > 100 ? 100 : checks;

				var messages = await msg.channel.bulkDelete(ntd, true);
				var content = this.client.CreateEmbed(msg.channel, `Successfully Pruned ${messages.size} Messages`, [{title: "Requested Quantity", description: args.number},{title:"Deleted Quantity", description: messages.size-1}], msg);
				
				if (typeof content == "string")
				{
					return msg.channel.send(content);
				}
				else
				{
					return msg.channel.send({ embed : content });
				}
				
				runTimes--;
			}
		}
		else
		{
			this.client.addRecord(msg.author, msg.guild, msg.author, args.reason, "Prune");

			var messages = await msg.channel.bulkDelete(2);
			var content = this.client.CreateEmbed(msg.channel, `Successfully Pruned 1 Message`, [{title: "Requested Quantity", content: args.number},{title:"Deleted Quantity", content: messages.size-1}], msg);
			
			if (typeof content == "string")
			{
				return msg.channel.send(content);
			}
			else
			{
				return msg.channel.send({ embed : content });
			}
		}
	}
};

function CheckForLink(content) {
	if (/^(http|https)/.test(content)) return true;

    return false;
}