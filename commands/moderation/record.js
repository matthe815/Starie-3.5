const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
const Discord = require('discord.js');

module.exports = class KickCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'record',
			group: 'moderation',
			memberName: 'record',
			guildOnly: true,
			guarded: true,
			userPermissions: ["MANAGE_MESSAGES"],
            description: 'Look up a warn record.',
			args: [{ key: "id", label: "case ID", type: "integer", prompt: "Please provide a vaild case ID" }]
		});
	}

	hasPermission(msg)
	{
		return msg.member.hasPermission("MANAGE_MESSAGES") || this.client.isOwner(msg.author)
	}
	
	async run(msg, args) {
        if (await this.client.validRecord(args.id))
        {
            var warn = await this.client.fetchRecord(args.id);

            var issuer = this.client.users.get(warn.issuer), recipient = this.client.users.get(warn.user), guild = this.client.guilds.get(warn.guild), action = warn.action;

			var content = this.client.CreateEmbed(msg.channel, warn.reason, [{title : "User", content: recipient.tag}, {title: "Action", content: `${warn.action.slice(0,1).toUpperCase()}${warn.action.slice(1)}`}], msg, this.decideColor(action), `Case #${warn.id}`);
			console.log(typeof content);

			if (typeof content == "string")
			{
				return msg.channel.send(content);
			}
			else
			{
				return msg.channel.send({ embed : content });
			}
        }
        else
        {
			var content = this.client.CreateEmbed(msg.channel, `The case ${args.id} doesn't exist!`, [], msg);
			console.log(typeof content);

			if (typeof content == "string")
			{
				return msg.channel.send(content);
			}
			else
			{
				return msg.channel.send({ embed : content });
			}
        }
    }
};

function decideColor(action)
{
    if (action == "ban") return "#FF0000"; else if (action == "kick" || action == "warn") return "#FFA500"; else return "#00F500";
}