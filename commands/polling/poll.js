﻿const { oneLine } = require('common-tags');
const Command = require('discord.js-commando').Command;
var Discord = require('discord.js');

module.exports = class ModChannelCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'poll',
			group: 'administration',
			memberName: 'poll',
			aliases: ["cp","createpoll","p"],
			description: 'Create a brand new poll for responses. (Incomplete)',
			throttling: {
				usages: 1,
				duration: 30
			},
			args: [{ key: "name", label: "Name", type: "string", prompt: "Select a name for the poll."}, { key: "time", label: "Time", type: "string", prompt: "Please select the amount of time you want to poll to go on for." }, { key: "mentionEveryone", type: "boolean", label: "Mention Everyone", prompt: "Please put whether or not you want to mention everyone. (Valid answers [True/False])"}, { key: "choices", type: "string", label: "choices", prompt: "Please input all choices for poll.", infinite: true }]
		});
	}

	hasPermission(msg)	
	{
		return msg.member.hasPermission("MANAGE_MESSAGES") || this.client.isOwner(msg.author);
	}
	
	async run(msg, args) {
		var poll = await msg.channel.send(`Generating Poll...`);
		var time = translateTime(args.time);
		var emb = new Discord.RichEmbed();
		emb.setAuthor(`${args.name} by ${msg.author.username}`, msg.author.avatarURL);
		var end = new Date(time["endingAt"]);
		emb.addField("Ending At", `${end.getHours()}:${end.getMinutes()} (EST)`);
		var choices = "";
		var num = 0;
		args.choices.forEach((choice) => {
			num++;
			var cnum = num;
			poll.react(translateEmote(cnum)).then(() => {
				choices = choices + `:${translateNumber(cnum)}: ${choice}\n`;
			}).then(() => {
				emb = new Discord.RichEmbed();
				emb.setAuthor(`${args.name} by ${msg.author.username}`, msg.author.avatarURL);
				emb.addField("Ending At", `${end.getHours()}:${end.getMinutes()} (EST)`);
				emb.addField("Choices", choices);
				poll.edit({ embed : emb });
				delete emb.fields[1];
			});
		});
		
		setTimeout(() => {
			poll.edit(new Discord.RichEmbed().setDescription("Ended"));
			var results = 0;
			var winner = "";
			var num = 0;
			poll.reactions.forEach((reaction) => {
				num++;
				if (reaction.count > results)
				{
					results = reaction.count-1;
					winner = args.choices[num-1];
				}
			});
			msg.channel.send(`${args.mentionEveryone ? "@everyone, " : ""}The poll ***"\`${args.name}\`"*** has concluded. And the winner is ${winner} with ${results} votes.`);
		}, time["time"]);
	}
};

function getNumber(string) {
	console.log(string);
	if (typeof number != "string") return 0;
	if (string == "one") return 0;
	if (string == "two") return 1;
	if (string == "three") return 2;
	if (string == "four") return 3;
	if (string == "five") return 4;
	if (string == "six") return 5;
	if (string == "seven") return 6;
	if (string == "eight") return 7;
	if (string == "nine") return 8;
}

function translateNumber(number) {
	if (typeof number != "number") return "octagonal_sign";
	if (number == 1) return "one";
	if (number == 2) return "two";
	if (number == 3) return "three";
	if (number == 4) return "four";
	if (number == 5) return "five";
	if (number == 6) return "six";
	if (number == 7) return "seven";
	if (number == 8) return "eight";
	if (number == 9) return "nine";
}

function translateEmote(number)
{
	if (typeof number != "number") return "🔟";
	if (number == 1) return "1⃣";
	if (number == 2) return "2⃣";
	if (number == 3) return "3⃣";
	if (number == 4) return "4⃣";
	if (number == 5) return "5⃣";
	if (number == 6) return "6⃣";
	if (number == 7) return "7⃣";
	if (number == 8) return "8⃣";
	if (number == 9) return "9⃣";

}

function translateTime(string) {
	
	var time = 0;
	
	while (string.length > 0)
	{
		var num = string.slice(0, string.search(/([a-z])/i));
		var iden = string.slice(string.search(/([a-z])/i),string.search(/([a-z])/i)+1);

		if (iden == "s")
		{
			time += 1000 * num;
			var length = num.toString().length + iden.toString().length;
			console.log(length);
			string = string.slice(length, string.length);
		}
		if (iden == "m")
		{
			time += 60000 * num;
			var length = num.toString().length + iden.toString().length;
			console.log(length);
			string = string.slice(length, string.length);
		}
		if (iden == "h")
		{
			time += 3600000 * num;
			var length = num.toString().length + iden.toString().length;
			console.log(length);
			string = string.slice(length, string.length);
		}
	}
	
	var endAt = new Date().getTime() + time;
	return { "time" : time, "endingAt" : endAt };
}