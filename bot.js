﻿const Discord = require('discord.js');
const Client = require("./classes/expandedClient");
const { Collection } = require('discord.js');
const sqlite = require('sqlite');
const request = require('request');
const fs = require('fs');
const path = require('path');
const sp = {};
const queue = [];
var newMessage = null;

const inviteList = new Discord.Collection();
const client = new Client({
    owner: ['207989356059688962'],
    invite: 'https://discord.gg/dwtR7pV'
}, this);

console.log(process.argv);

client.registry

client.registry
    // Registers your custom command groups
    .registerGroups([
        ['general', 'General Commands'],
        ['moderation', 'Moderation Commands', true],
        ['administration', 'Administration Commands', true],
        ['fetching', 'Fetching Commands'],
        ['accolades', 'Accolade Commands'],
        ['economy', 'Economy Commands'],
        ['owner', 'Owner Commands', true],
        ['polling', 'Polling Commands'],
        ['music', 'Music Commands'],
        ['summarization', 'Summary Commands'],
        ['rpg', 'RPG Commands'],
        ['survival', 'Survival Commands'],
        ['tags', 'Tag Commands']
    ])

    // Registers all built-in groups, commands, and argument types
    .registerDefaults()
    .registerType(require('./types/guild'))
    .registerType(require('./types/accolade'))
    .registerType(require('./types/textchannel'))
    .registerType(require('./types/time'))

    // Registers all of your commands in the ./commands/ directory
    .registerCommandsIn(path.join(__dirname, 'commands'));

client.setProvider(
    sqlite.open(path.join(__dirname, 'settings.sqlite3')).then(db => new require("discord.js-commando").SQLiteProvider(db))
).catch(console.error);

client.on("messageUpdate", (om, nm) => {
    if (nm.author == client.user || nm.author.bot) return;

    // Check the content for any violations.
    try {
        moderate(nm);
    } catch (e) { console.log(e); }
});

client.on("ready", async () => {
    // Unload all of the commands to be over-written.
    client.registry.unregisterCommand(client.registry.commands.get("eval"));
    client.registry.unregisterCommand(client.registry.commands.get("help"));
    client.registry.unregisterCommand(client.registry.commands.get("enable"));
    client.registry.unregisterCommand(client.registry.commands.get("prefix"));
    client.registry.unregisterCommand(client.registry.commands.get("disable"));
    client.registry.unregisterCommand(client.registry.commands.get("ping"));
    client.registry.registerCommandsIn(path.join(__dirname, 'utils'));

    // Load all of the records from the DB into the cache.
    await client.Load();

    // Post the current guild stats to Discord Bot List.
    dbl.postStats(client.guilds.size);

    // Set the activity to the stock one.
    await client.user.setActivity(`Booting up...`);
});

client.on("message", async (message) => {
    if (message.author == client.user || message.author.bot) return;

    // Check the content for any violations.
    try {
        moderate(message);
    } catch (e) { console.log(e); }
});

function moderate(message) {
    // Grab the necessary values and put them into variables.
    var msg = message.content,
        msgLow = message.content.toLowerCase(),
        author = message.member,
        guild = message.guild;

    // Whether or not the message is invalid.
    var invalid = false;

    // {============PORN CHECKER============} \\
    var msgFiltered = msgLow.replace(/[^a-z0-9./ ]/gi, "");
    var words = msgFiltered.split(" ");

    words.forEach((part) => {
        if (isLink(part)) {
            console.log(part);
            var req = `https://api.mywot.com/0.4/public_link_json2?hosts=${part}/&callback=process&key=b7fabd9046e56a44d6aaf4f70f2bed17b76d648e`;
            console.log(req);
            request(req, function(error, response, body) {
                console.log('error:', error); // Print the error if one occurred
                console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                console.log('body:', String(body).slice(8, -1)); // Print the HTML for the Google homepage
                var json = JSON.parse(String(body).slice(8, -1));

                for (var j in json) {
                    if (json[j].categories && json[j].categories["401"]) {
                        client.Warn(message, "Do NOT Post Links to Adult Content");

                        return message.delete(0);
                    }
                }
            });
        }
    });

    // Return if you're a Moderator.
    //if (message.member.hasPermission("MANAGE_MESSAGES") || client.isOwner(message.author)) return;

    // {============DISCORD CHECKER============} \\
    if (message.content.includes("discord.gg/") || message.content.includes("discordapp.com/invite/")) {

        // If there's no invite list, obtain it.
        if (!inviteList.has(message.guild.id)) {
            message.guild.fetchInvites().then((invites) => {
                inviteList.set(message.guild.id, new Map());
                invites.forEach((invite) => {
                    inviteList.get(message.guild.id).set(invite.code, invite);
                });
            });
        }

        // Obtain the information required.
        var invites = inviteList.get(message.guild.id);
        var link = "";

        // Check if the bit contains the particular information.
        if (msgLow.includes("discord.gg/")) {
            var link = msg.slice(msgLow.search('discord.gg/') + 11, msgLow.search('discord.gg/') + 11 + 7);
        } else if (msgLow.includes("discordapp.com/invite/")) {
            var link = msg.slice(msgLow.search('discordapp.com/invite/') + 22, msgLow.indexOf("/", msgLow.search('discordapp.com/invite/')));
        }

        console.log(link);

        // If the invite doesn't exist, assume it's bad.
        if (invites && !invites.has(link)) {
            client.Warn(message, "Do NOT Advertise Your Server Here");

            message.delete(0);
        }
    }

    // {============ANTI MENTION SPAM==============} \\
    if (message.mentions && message.mentions.members.size > 4)
    {
        try {
            client.Warn(message, "Do NOT Spam Mentions");
        } catch (e) {
            console.error(e);
        }
    }

    // {============WORD BLACKLIST=================} \\
    var guildBL = client.blacklist.get(message.guild.id);

    if (guildBL)
    {
        words.forEach((word) => {
            console.log(word);
            if (guildBL.includes(word))
            {
                client.Warn(message, `Please do not use blacklisted words. Word in question: \`${word}\`.`);
            }
            else if (guildBL.includes(word.replace(/[^a-z0-9]/gi, "")))
            {
                client.Warn(message, `Please do not use blacklisted words. Word in question: \`${word}\`.`);
            }
        });
    }

    /* {============WORD WHITELIST=================} \\
    var guildWL = client.whitelist.get(message.guild.id);

    if (guildWL && !message.member.roles.find((role) => { return role.name == "Bypass" }))
    {
        words.forEach((word) => {
            console.log(word);
            if (!guildWL.includes(word))
            {
                client.Warn(message, `Please use only white-listed words. Valid phrases ${guildWL.join(", ")}`);
                return true;
            }
        });
    }*/

    // {================Spam Filter==================} \\
    if (!message.member.messages || !message.member.recent)
    {
        message.member.messages = new Collection();
        message.member.recent = {
            count: 0,
            time: 0
        }
    }
    
    if (message.member.recent.time + 5000 < new Date().getTime())
    {
        message.member.recent.count = 0;
        message.member.recent.time = new Date().getTime();
        message.member.messages = new Collection();
    }

    if (message.member.recent.count == 3) { message.channel.bulkDelete(message.member.messages); client.Warn(message, "Do not post a large number of unwanted (Spam) messages"); }
    if (message.member.recent.count == 6) { message.channel.bulkDelete(message.member.messages); client.Mute(message, "Do not post a large number of unwanted (Spam) messages [MUTE]"); }
    message.member.messages.set(message.id, message);
    message.member.recent.count++;

    console.log(invalid);

    return invalid;
}

client.on("debug", (e) => {
    if (!client.debug) return;

    console.log(e);
});

function isLink(link) {
    if (link.startsWith("http") || link.startsWith("/") || link.startsWith("www") || link.endsWith("xxx") || link.endsWith("com") || link.endsWith("org") || link.endsWith("net")) return true;
    return false;
}

function onExit()
{
    client.destroy();
}

client.login(process.argv[2]);