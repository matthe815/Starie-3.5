const Accolade = require("../classes/accolade");
const Discord = require("discord.js");

class StarieAccolade extends Accolade {
    constructor(client)
    {
        super(client, {
            id: 1,
            name: "Starie Fan",
            description: "An award for being in Starie's dev server.",
            icon: ":smiley:"
        });
    }

    async unlock(msg)
    {
        var user = msg.author;
        
        if (client.guilds.get("305815150735392788").members.has(msg.author.id))
        {
            this.client.unlockAccolade(user, this.id);
            var re = new Discord.RichEmbed();
            re.setAuthor(msg.author.username, msg.author.avatarURL);
            re.setDescription("Accolade `Starie Fan` Successfully Unlocked");
            re.setTimestamp(new Date());
            msg.say({ embed : re });
        }
        else
        {
            var re = new Discord.RichEmbed();
            re.setAuthor("Error");
            re.setColor("#FF0000");
            re.setDescription("You Must Be In Starie R&D To Unlock This Accolade");
            re.setTimestamp(new Date());
            msg.say({ embed : re });
        }
    }
}

module.exports = TestAccolade;