const ArgumentType = require('discord.js-commando').ArgumentType;
const disambiguation = require('discord.js-commando').util.disambiguation;
const escapeMarkdown = require('discord.js').escapeMarkdown;

class GuildArgumentType extends ArgumentType {
	constructor(client) {
		super(client, 'guild');
	}

	async validate(value, msg) {
		const search = value.toLowerCase();
		let guilds = this.client.guilds.filterArray(guildFilterInexact(search));
		if(guilds.length === 0) return false;
		if(guilds.length === 1) return guilds[0];
		const exactGuilds = guilds.filter(guildFilterExact(search));
		if(exactGuilds.length === 1) return guilds[0];
		if(exactGuilds.length > 0) guilds = exactGuilds;
		return guilds.length <= 15 ?
			`${disambiguation(
				guilds.map(guild => `${escapeMarkdown(guild.name)}`), 'guilds', null
			)}\n` :
			'Multiple guilds found. Please be more specific.';
	}

	parse(value, msg) {
		const search = value.toLowerCase();
		const guilds = this.client.guilds.filterArray(guildFilterInexact(search));
		if(guilds.length === 0) return null;
		if(guilds.length === 1) return guilds[0];
		const exactGuilds = guilds.filter(guildFilterExact(search));
		if(exactGuilds.length === 1) return guilds[0];
		return null;
	}
}

function guildFilterExact(search) {
	return guild => guild.name.toLowerCase() === search;
}

function guildFilterInexact(search) {
	return guild => guild.name.toLowerCase().includes(search);
}

module.exports = GuildArgumentType;
