const ArgumentType = require('discord.js-commando').ArgumentType;
const disambiguation = require('discord.js-commando').util.disambiguation;
const escapeMarkdown = require('discord.js').escapeMarkdown;

class TimeArgumentType extends ArgumentType {
	constructor(client) {
		super(client, 'time');
	}

	async validate(value, msg) {
        if (typeof value != "string") return false;
        return true;
	}

	parse(value, msg) {
        return value;
	}
}

module.exports = TimeArgumentType;
