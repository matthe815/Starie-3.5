const ArgumentType = require('discord.js-commando').ArgumentType;
const disambiguation = require('discord.js-commando').util.disambiguation;
const escapeMarkdown = require('discord.js').escapeMarkdown;

class ChannelArgumentType extends ArgumentType {
	constructor(client) {
		super(client, 'textchannel');
	}

	validate(value, msg) {
        const matches = value.match(/^(?:<#)?([0-9]+)>?$/);
        const textchannels = msg.guild.channels.filter((channel) => { return channel.type == 'text' });
		if(matches) return textchannels.has(matches[1]);
		const search = value.toLowerCase();
		let channels = textchannels.filterArray(nameFilterInexact(search));
		if(channels.length === 0) return false;
		if(channels.length === 1) return true;
		const exactChannels = textchannels.filter(nameFilterExact(search));
		if(exactChannels.length === 1) return true;
		if(exactChannels.length > 0) channels = exactChannels;
		return `${disambiguation(channels.map(chan => escapeMarkdown(chan.name)), 'channels', null)}\n`;
	}

	parse(value, msg) {
        const matches = value.match(/^(?:<#)?([0-9]+)>?$/);
        const textchannels = msg.guild.channels.filter((channel) => { return channel.type == 'text' });
		if(matches) return textchannels.get(matches[1]) || null;
		const search = value.toLowerCase();
		const channels = textchannels.filterArray(nameFilterInexact(search));
		if(channels.length === 0) return null;
		if(channels.length === 1) return channels[0];
		const exactChannels = textchannels.filter(nameFilterExact(search));
		if(exactChannels.length === 1) return channels[0];
		return null;
	}
}

function nameFilterExact(search) {
	return thing => thing.name.toLowerCase() === search && thing.type == 'text';
}

function nameFilterInexact(search) {
	return thing => thing.name.toLowerCase().includes(search) && thing.type == 'text';
}

module.exports = ChannelArgumentType;
