const ArgumentType = require('discord.js-commando').ArgumentType;
const disambiguation = require('discord.js-commando').util.disambiguation;
const escapeMarkdown = require('discord.js').escapeMarkdown;

class AccoladeArgumentType extends ArgumentType {
	constructor(client) {
		super(client, 'accolade');
	}

	async validate(value, msg) {
		const search = value.toLowerCase();
		let accolades = this.client.accolades.filterArray(accoladeFilterInexact(search));
		if(accolades.length === 0) return false;
		if(accolades.length === 1) return accolades[0];
		const exactaccolade = accolades.filter(accoladeFilterExact(search));
		if(exactaccolade.length === 1) return accolades[0];
		if(exactaccolade.length > 0) accolades = exactaccolade;
		return accolades.length <= 15 ?
			`${disambiguation(
				accolades.map(accolade => `${escapeMarkdown(accolade.name)}`), 'accolade', null
			)}\n` :
			'Multiple accolade found. Please be more specific.';
	}

	parse(value, msg) {
		const search = value.toLowerCase();
		const accolade = this.client.accolades.filterArray(accoladeFilterInexact(search));
		if(accolade.length === 0) return null;
		if(accolade.length === 1) return accolade[0];
		const exactaccolade = accolade.filter(accoladeFilterExact(search));
		if(exactaccolade.length === 1) return accolade[0];
		return null;
	}
}

function accoladeFilterExact(search) {
	return accolade => accolade.name.toLowerCase() === search;
}

function accoladeFilterInexact(search) {
	return accolade => accolade.name.toLowerCase().includes(search);
}

module.exports = AccoladeArgumentType;
