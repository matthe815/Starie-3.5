const Item = require('./item.js');
const Material = require('./material.js');

class Character {
    constructor(client, user, name="", gender=false)
    {
        this.client = client;
        this.user = user;
        this.name = name;
        this.gender = gender;
        this.equipment = {
            LeftHand: null,
            RightHand: null,
            Helmet: null,
            Chestplate: null,
            Gloves: null,
            Belt: null,
            Pants: null,
            Boots: null
        };
    }

    get might()
    {
        var might = 1;
        var eq = this.equipment;

        for (const item in eq)
        {
            if (eq[item]) might += eq[item]["might"];
        }

        return might;
    }

    async resolveEquipment(items)
    {
        for (const item in items) {
            if (items[item] != null)
            {
                let equipable = new Item(false);
                equipable.name = items[item]["name"];
                equipable.might = items[item]["might"];
                equipable.type = items[item]["type"];
                equipable.material = new Material(items[item]["material"]);
                equipable.material.name = items[item]["material"]["name"];
                equipable.level = items[item]["level"];
                equipable.material.might = items[item]["material"]["might"];
                this.equip(equipable.type, equipable, false);
            }
            continue;
        };
    }

    getEquipment()
    {
        var list = [];
        var eq = JSON.parse(this.stringifyEquipment());

        for (const item in eq)
        {
            list.push(`${eq[item]["name"]}`);
        }

        return list.join(",\n");
    }

    getMight()
    {
        var list = [];
        var eq = JSON.parse(this.stringifyEquipment());

        for (const item in eq)
        {
            list.push(`${eq[item]["name"]}: ${eq[item]["might"]}`);
        }
  
        return list.join(", ");
    }

    stringifyEquipment()
    {
        let equip = {};
        if (this.equipment.LeftHand) equip["lefthand"] = this.equipment.LeftHand;
        if (this.equipment.RightHand) equip["righthand"] = this.equipment.RightHand;
        if (this.equipment.Helmet) equip["helmet"] = this.equipment.Helmet;
        if (this.equipment.Chestplate) equip["chestplate"] = this.equipment.Chestplate;
        if (this.equipment.Gloves) equip["gloves"] = this.equipment.Gloves;
        if (this.equipment.Belt) equip["belt"] = this.equipment.Belt;
        if (this.equipment.Pants) equip["pants"] = this.equipment.Pants;
        if (this.equipment.Boots) equip["boots"] = this.equipment.Boots;

        return JSON.stringify(equip);
    }

    equip(type, object, save=true)
    {
        console.log(`Equiping ${object.name} of type ${object.type}`);
        if (type == "lefthand") this.equipment.LeftHand = object;
        if (type == "righthand") this.equipment.RightHand = object;
        if (type == "helmet") this.equipment.Helmet = object;
        if (type == "chestplate") this.equipment.Chestplate = object;
        if (type == "gloves") this.equipment.Gloves = object;
        if (type == "belt") this.equipment.Belt = object;
        if (type == "pants") this.equipment.Pants = object;
        if (type == "boots") this.equipment.Boots = object;
        if (save) this.save();
    }

    async save()
    {
        var query = `UPDATE \`rpgCharacters\` SET \`equipment\`='${this.stringifyEquipment()}' WHERE \`user\`="${this.user.user}"`;
        this.client.provider.db.run(query);
    }

    unequip(type, save=true)
    {
        if (type == "lefthand") { this.equipment.LeftHand = null; }
        if (type == "righthand") { this.equipment.RightHand = null; }
        if (type == "helmet") { this.equipment.Helmet = null; }
        if (type == "chestplate") { this.equipment.Chestplate = null; }
        if (type == "gloves") { this.equipment.Gloves = null; }
        if (type == "belt") { this.equipment.Belt = null; }
        if (type == "pants") { this.equipment.Pants = null; }
        if (type == "boots") { this.equipment.Boots = null; }
        if (save) this.save();
    }

    equipped(type)
    {
        if (type == "lefthand") { if (this.equipment.LeftHand) return true; }
        if (type == "righthand") { if (this.equipment.RightHand) return true; }
        if (type == "helmet") { if (this.equipment.Helmet) return true; }
        if (type == "chestplate") { if (this.equipment.Chestplate) return true; }
        if (type == "gloves") { if (this.equipment.Gloves) return true; }
        if (type == "belt") { if (this.equipment.Belt) return true; }
        if (type == "pants") { if (this.equipment.Pants) return true; }
        if (type == "boots") { if (this.equipment.Boots) return true; }

        return false;
    }
}

module.exports = Character;