const Permissions = require('./permissions');

class PermissionsGroup {
	constructor(options = {})
	{
		this.permissions = options.permissions;
	}
}

module.exports = PermissionsGroup;