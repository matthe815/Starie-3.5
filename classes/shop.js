const Item = require('./item.js');

class Shop {
    constructor(client, guild)
    {
        this.items = [];
        this.guild = guild;
        this.client = client;
        this.time = 0;
        this.generate();
    }

    generate()
    {
        while (this.items.length < Math.floor(Math.random() * 5)) this.items.push(new Item());
        this.time = new Date().getTime();
    }

    outOfTime()
    {
        if ((new Date().getTime() - this.time) > 1000000) return true;
        return false;
    }

    has(name)
    {
        this.items.forEach((item) => {
            if (item.name == name) return true;
        })

        return false;
    }

    get(name)
    {
        this.items.forEach((item) => {
            if (item.name == name) return item;
        })

        return false;
    }
}

module.exports = Shop;