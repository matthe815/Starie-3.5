class Material {
    constructor(random=true)
    {
        let names = ["Lithium", "Sodium", "Potassium", "Rubidium", "Cesium", "Francium", "Beryllium", "Magnesium", "Calcium", "Strontium", "Barium", "Radium", "Aluminum", "Gallium", "Indium", "Tin", "Thallium", "Lead", "Bismuth", "Tennessine", "Iron", "Cobalt", "Titanium", "Chromium", "Manganese", "Copper", "Zinc", "Nickel", "Gold", "Iron", "Darmstadtium"];
        this.name = names[Math.floor(Math.random() * names.length)];
        this.might = this.name.length * Math.floor(Math.random() * 3)+1;
    }
}

module.exports = Material;