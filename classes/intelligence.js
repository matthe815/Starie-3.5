class Intelligence {
	constructor(client) {
		this.client = client;
	}
	
	async ask(query)
	{
		var r = await this.client.provider.db.all(`SELECT * FROM \`intelligence\` WHERE \`question\`="${query}"`);
		
		var responses = [];
		
		for (const row of r)
		{
			try {
				responses.push(row.response);
			} catch (e) {
				console.log(e);
				continue;
			}
		}
		
		if (responses.length == 0) return "I don't know what to say...";
		
		return responses[Math.floor(Math.random() * responses.length)];
	}
	
	async learn(query, response)
	{
		return this.client.provider.db.run(`INSERT INTO \`intelligence\`(\`question\`, \`response\`) VALUES("${safeString(query)}", "${safeString(response)}")`);
	}
	
}

function safeString(string)
{
	string = String(string);
	return string.replace(/"/g, "'");
}


module.exports = Intelligence;