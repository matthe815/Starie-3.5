class CCommand {
    constructor(id, name, memberName, response, guild)
    {
        this.id = id;
        this.name = name;
        this.memberName = memberName;
        this.response = response;
        this.guild = guild;
    }
}

module.exports = CCommand;