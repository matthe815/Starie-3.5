const Permissions = require('./permissions');
const PermissionsGroup = require('./permissionsgroup');
const Shop = require('./shop.js');

class GuildData {
	constructor(id, guild, modChannel, logDeleting, deleteChannel, serverLog, welcomeMessages, welcomeChannel, welcomeMessage, muteRole, messageLimit, permissions, verified, stariePerms, client)
	{
		this.id = id;
		this.guild = guild;
		this.modChannel = modChannel;
		this.logDeletes = logDeleting;
		this.deleteChannel = deleteChannel;
		this.serverLog = serverLog;
		this.muteRole = muteRole;
		this.verified = verified;
		this.messageLimit = messageLimit;
		this.welcomeChannel = welcomeChannel;
		this.welcomeMessage = welcomeMessage;
		this.welcomeMessages = welcomeMessages;
		this.permissions = permissions;
		this.client = client;
		this.stariePerms = stariePerms;
		this.shop = null;
	}
	
	newShop()
	{
		this.shop = new Shop(this.client, this);
	}

	hasPermission(permission, user)
	{
		var guild = this.client.guilds.get(this.guild);
		var member = guild.member(user);
		if (!guild.members.has(user.id)) return false;

		if (this.client.isOwner(user) || member.hasPermission("ADMINISTRATOR"))
		{
			if (this.client.isOwner(user) || this.permissions[0].permissions.all_perms) return true;
			if (permission == "delete_logs" && this.permissions[0].permissions.delete_logs) return true;
			if (permission == "pin_logs" && this.permissions[0].permissions.pin_logs) return true;
			if (permission == "view_logs" && this.permissions[0].permissions.view_logs) return true;
			if (permission == "view_panel" && this.permissions[0].permissions.view_panel) return true;
			return false;
		}
		else if (member && member.hasPermission("MANAGE_MESSAGES"))
		{
			if (this.permissions[1].permissions.all_perms) return true;
			if (permission == "delete_logs" && this.permissions[1].permissions.delete_logs) return true;
			if (permission == "pin_logs" && this.permissions[1].permissions.pin_logs) return true;
			if (permission == "view_logs" && this.permissions[1].permissions.view_logs) return true;
			if (permission == "view_panel" && this.permissions[1].permissions.view_panel) return true;
			return false;
		}
		else
		{
			return false;
		}
	}
}

module.exports = GuildData;