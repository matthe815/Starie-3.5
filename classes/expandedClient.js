const CClient = require("discord.js-commando").Client;
const Collection = require("discord.js").Collection;
const CCommand = require("./ccommand.js");
const Discord = require('discord.js');
const Accolade = require("./accolade");
const UserData = require("./userdata");
const GuildData = require("./guilddata");
const Intelligence = require("./intelligence");
const Permissions = require('./permissions');
const PermissionsGroup = require('./permissionsgroup');
const WordBase = require('./wordbase');
const Battle = require('./battle');

class Client extends CClient
{
	constructor(clientOptions, client)
	{
		super(clientOptions);
		this.client = client;
		this.accolades = new Collection();
		this.userData = new Collection();
		this.guildData = new Collection();
		this.intelligence = new Intelligence(this);
		this.cCommands = new Collection();
		this.wordBase = new WordBase(this);
		this.calls = new Collection();
		this.battles = new Collection();
		this.rolePermissions = new Collection();
		this.blacklist = new Collection();
		this.whitelist = new Collection();
		this.debug = false;
		this.records = 0;
		this.permissions = { "channels" : { "modify" : { "moderator" : "MANAGE_MODCHANNEL", "deletion" : "MANAGE_DELETECHANNEL" }}, "moderation" : { "manage" : { "unban" : "UNBAN_MEMBERS", "warn" : "WARN_MEMBERS", "kick" : "KICK_MEMBERS", "ban" : "BAN_MEMBERS", "prune" : "PRUNE_MESSAGES" } }, "administration" : { "permissions" : { "modify" : "MANAGE_PERMISSIONS", "bypass" : "BYPASS_PERMISSIONS", "view" : "VIEW_PERMISSIONS" }}};
		this.permissionNodes = { "MANAGE_MODCHANNEL" : "channels.modify.moderator", "MANAGE_DELETECHANNEL" : "channels.modify.deletion", "UNBAN_MEMBERS" : "moderation.manage.unban", "WARN_MEMBERS" : "moderation.manage.warn", "KICK_MEMBERS" : "moderation.manage.kick", "BAN_MEMBERS" : "moderation.manage.ban", "PRUNE_MESSAGES" : "moderation.manage.prune", "MANAGE_PERMISSIONS" : "administration.permissions.modify", "BYPASS_PERMISSIONS" : "administration.permissions.bypass", "VIEW_PERMISSIONS" : "administration.permissions.view" }
	}

	ValidatePermission(group, msg)
	{
		// Break down into parts.
		var groups = group.split("."), currentPage = msg.client.permissions;

		// If it ends with a wildcard, validate it.
		if (group.endsWith("*")) return true;

		// Go through the parts and verify that it exists.
		groups.forEach((group) => {
			currentPage = currentPage[group];
		});

		// Make sure it ends on a permission, and not a category.
		if (typeof currentPage != "string") return false;

		// Return the found permission.
		return true;
	}

	ListNodes()
	{
		var perms = this.permissionNodes;
		var nodes = [];

		Object.keys(perms).forEach((key) => {
			nodes.push(perms[key]);
		});

		return nodes;
	}

	IsHigherThan(role)
	{

	}

	FetchAllPermissions(group)
	{
		if (!group) return null;

		console.log("Getting all permissions for " + group);

		var toAdd = [];
		var sifted = group;

		while (Object.keys(sifted).length > 0)
		{
			var nextkey = Object.keys(sifted)[0];

			if (typeof sifted[nextkey] == "string")
			{
				toAdd.push(sifted[nextkey]);
			}
			else
			{
				Object.assign(sifted, sifted[nextkey]);
			}

			delete sifted[nextkey];

		}

		console.log(toAdd);

		return toAdd.join(",");
	}

	GetPermission(group, msg)
	{
		// Break down into parts.
		var groups = group.split("."), currentPage = msg.client.permissions;

		// Go through the parts and verify that it exists.
		groups.forEach((group) => {
			if (group == "*") {
				currentPage = msg.client.FetchAllPermissions(currentPage);
			}else{
				currentPage = currentPage[group];
			}
		});

		// Make sure it ends on a permission, and not a category.
		if (typeof currentPage != "string") return null;

		// Return the found permission.
		return currentPage;
	}

	async fetchRecords()
	{
		var records = await this.provider.db.all("SELECT * FROM `modRecord`");
		this.records = records.length;
		return records.length;
	}

	async FetchRecords() { return await this.fetchRecords(); }

	async addAccolade(accolade)
	{
		this.accolades.set(accolade.id, accolade);
		this.provider.db.run(`INSERT INTO accolades(\`accoladeId\`, \`name\`, \`description\`, \`icon\`, \`conditions\`) VALUES(${accolade.id}, "${accolade.name}", "${accolade.description}", "${accolade.icon}", "")`);
		return accolade;
	}
	
	async LoadAccolades(){}

	CreateEmbed(channel, description, fields, message, color=null, footer=null, isGuild=true) {

		// Verify that the user has permissions to send embeds in this channel.
		if (channel.permissionsFor(this.user).has("EMBED_LINKS")) {

			// Create an embed.
			var Embed = new Discord.RichEmbed(), user = message.author, guild = message.guild;

			if (!isGuild) Embed.setAuthor(user.username, user.displayAvatarURL); else Embed.setAuthor(guild.name, guild.iconURL);
			if (color) Embed.setColor(color);
			Embed.setDescription(description);
			
			fields.forEach((field) => {
				Embed.addField(field.title, field.description, field.inline ? field.inline : false);
			});
			
			if (footer) Embed.setFooter(footer);

			Embed.setTimestamp(new Date());

			return Embed;

		}
		else
		{
			var fieldList = "";
			
			fields.forEach((field) => {
				fieldList+=` ${field.title}: ${field.description}`;
			});

			return `${description},${fieldList}`;
		}


	}

	async FetchBlacklist()
	{
		console.log("Loading guild information");
		var users = await this.provider.db.all('SELECT * FROM blacklist');
		for(const row of users) {
			let word;
			try {
				console.log(`Loading ${row.guild}'s ${row.text}.`);
				word = row;
				console.log(`Loaded ${row.guild}'s ${row.text}`);
			} catch(err) {
				this.emit('warn', `SQLiteProvider couldn't parse the settings stored for word ${row.text}.`);
				continue;
			}

			if (!this.blacklist.has(word.guild)) this.blacklist.set(word.guild, []);
			this.blacklist.get(word.guild).push(word.text);
		}
	}
	
	async FetchWhitelist()
	{
		console.log("Loading guild information");
		var users = await this.provider.db.all('SELECT * FROM whitelist');
		for(const row of users) {
			let word;
			try {
				console.log(`Loading ${row.guild}'s ${row.text}.`);
				word = row;
				console.log(`Loaded ${row.guild}'s ${row.text}`);
			} catch(err) {
				this.emit('warn', `SQLiteProvider couldn't parse the settings stored for word ${row.text}.`);
				continue;
			}

			if (!this.whitelist.has(word.guild)) this.whitelist.set(word.guild, []);
			this.whitelist.get(word.guild).push(word.text);
		}
	}


	PermissionsFor(member)
	{
		// Permissions list.
		let permissionsUC = [];

		// Loop through all roles that the member has.
		member.roles.forEach((role) => {
			
			// Fail if this role doesn't have a permission set.
			if (!this.rolePermissions.has(role.id)) return;

			// Get the permissions set from the role.
			this.rolePermissions.get(role.id).forEach((permission) => {
				
				// Make sure the current permission isn't in the list to avoid duplicates.
				if (permissionsUC.indexOf(permission) != -1) return;

				// Push the permission into the list.
				permissionsUC.push(permission);

			});

		});

		// Return all of the accounted permissions.
		return permissionsUC;
	}

	Differentiate(dp, sp, member)
	{
		var usesDP = this.guildData.get(member.guild.id).stariePerms;

		if (!usesDP)
		{
			if (member.hasPermission(dp)) return true;
			return false;
		}
		else
		{
			if (member.guild.owner == member) return true;
			return this.HasPermission(sp, member);
			return false;
		}
	}

	HasPermission(permission, member)
	{
		// If fetchPermissions is true, run the fetch process and use that list instead.
		let list = this.PermissionsFor(member);

		// Return whether or not the permission is present in the list.
		return list.indexOf(permission) != -1;
	}

	SetPermissions(role, permissions)
	{
		console.log(`Changing ${role}'s permissions to ${JSON.stringify(permissions)}`);

		// Check if the role is in the DB via the cache.
		if (this.rolePermissions.has(role))
		{
			// Update the record for the role.
			this.provider.db.run(`UPDATE role_permissions SET \`permissions\`=? WHERE \`role\`=?`, [JSON.stringify(permissions), role]);
		}
		else
		{
			// Create the record for the role.
			let pl = {permissions};

			console.log(`INSERT INTO role_permissions(role, permissions) VALUES("${role}",'${JSON.stringify(permissions)}')`);
			this.provider.db.run(`INSERT INTO role_permissions(role, permissions) VALUES(?,?)`, [role, JSON.stringify(permissions)]);
		}

		this.rolePermissions = new Collection();
		this.LoadPermissions();
	}

	ParsePermissions(permissions)
	{
		// An array of valid permissions.
		let permissionsUC = [];

		// Get the permissions.
		permissions = permissions["permissions"];

		// Get the permissions set from the role.
		permissions.forEach((permission) => {
				
			// Make sure the current permission isn't in the list to avoid duplicates.
			if (permissionsUC.indexOf(permission) != -1) return;

			// Push the permission into the list.
			permissionsUC.push(permission);

		});

		return permissionsUC;
	}

	loginAs(user, victim)
	{
		// Check if the user has permission to do so.
		if (victim.allows(user)) user.is = victim;
	}

	async addBattle(opp, msg)
	{
		var battle = new Battle({ id: this.battles.size, opponent: { id: opp.id, character: this.userData.get(opp.id).character}, challenger: { id: msg.author.id, character: this.userData.get(msg.author.id).character } }, `this.client`);
		this.battles.set(battle.id, battle);
		return battle;
	}
	
	async unlockAccolade(user, accolade)
	{
		await this.provider.db.run(`UPDATE \`users\` SET \`accolades\`="${client.userData.get(user.id).stringifyAccolades()}" WHERE \`user\`=${user.id}`);
		return;
	}

	async Load()
	{
		console.log("Beginning the start up process");

		await this.LoadUsers();
		await this.LoadPermissions();
		await this.LoadGuilds();
		await this.LoadAccolades();
		await this.LoadCommands();
		await this.FetchRecords();
		await this.FetchBlacklist();
		await this.FetchWhitelist();

	}

	async LoadUsers()
	{
		console.log("Initalizing user information.");
		var users = await this.provider.db.all('SELECT * FROM userSettings');
		for(const row of users) {
			let user;
			try {
				user = new UserData(row.id, row.user, row.prefix, row.panelAccess, row.money, row.dailyTime, this);
				user.loadCharacter();
			} catch(err) {
				this.emit('warn', `SQLiteProvider couldn't parse the settings stored for guild ${row.name}.`);
				continue;
			}

			const userId = row.user !== '0' ? row.user : 'global';
			this.userData.set(userId, user);
		}
		
		this.emit("usersFetched", (this.userData));
	}

	async addCommand(name, response, msg)
	{
		console.log(`Creating a new command with the name ${name} and response ${response}.`);
		this.provider.db.run(`INSERT INTO \`custom-commands\`(name, response, guild, creator, createdAt) VALUES(?, ?, ?, ?, ?)`, name, response, msg.guild.id, msg.author.id, new Date().getTime());
		
		if (!this.cCommands.has(msg.guild.id)) this.cCommands.set(msg.guild.id, new Collection());
		this.cCommands.get(msg.guild.id).set(name, { "response": response });
	}
	
	async LoadCommands()
	{
		console.log("Initalizing custom commands.");
		var commands = await this.provider.db.all('SELECT * FROM `custom-commands`');
		for (const row of commands)
		{
			let command;
			try {
				command = new CCommand(row.id, row.name, row.memberName, row.response, row.guild);
			} catch (err) {
				this.emit('warn', `SQLiteProvider couldn't parse the settings stored for command ${row.memberName}`);
				continue;
			}

			const commandId = row.guild !== '0' ? row.guild : 'global';

			if (!this.cCommands.has(row.guild)) this.cCommands.set(row.guild, new Collection());
			this.cCommands.get(row.guild).set(command.memberName, command);
		}
	}

	async LoadGuilds()
	{
		console.log("Initalizing guild information.");
		var users = await this.provider.db.all('SELECT * FROM guildSettings');
		for(const row of users) {
			let guild;
			try {
				guild = new GuildData(row.id, row.guild, row.modChannel, row.logDeletes, row.deleteChannel, row.serverLog, row.welcomeMessages, row.welcomeChannel, row.welcomeMessage, row.mutedRole, row.messageLimit, processPerms(row.permissions), row.verified, row.stariePerms, this);
			} catch(err) {
				this.emit('warn', `SQLiteProvider couldn't parse the settings stored for guild ${row.name}.`);
				continue;
			}

			const guildId = row.guild !== '0' ? row.guild : 'global';
			this.guildData.set(guildId, guild);
		}
		console.log("Initalized guild information.");

		this.guilds.forEach((guild) =>  {
			guild.data = this.guildData.get(guild.id);
		});

		this.emit("guildsFetched", (this.guildData));
	}

	async LoadPermissions()
	{
		console.log("Loading permission information");
		var permissionset = await this.provider.db.all('SELECT * FROM role_permissions');
		for(const row of permissionset) {
			let permissions;
			try {
				permissions = row.permissions;
			} catch(err) {
				this.emit('warn', `SQLiteProvider couldn't parse the settings stored for guild ${row.role}.`);
				continue;
			}

			this.rolePermissions.set(row.role, this.ParsePermissions(JSON.parse(permissions)));
		}

		console.log("Loaded permission information");
		
		this.emit("permissionsFetched", (this.rolePermissions));		
	}
	
	async addUser(user)
	{
		console.log("Adding user to database.");
		this.provider.db.run(`INSERT INTO userSettings(user, money) VALUES("${user.id}", 0)`);
		this.userData.set(user["id"], new UserData(this.userData.size+1, user.id, null, 0, 0, 0, this));
		this.emit("userAdded", user, this.userData.get(user.id));
	}

	async addUserFromFile(user)
	{
		console.log("Adding user to database.");
		this.provider.db.run(`INSERT INTO userSettings(user, panelAccess, money) VALUES("${user.user}","${user.panelAccess}", "${user.money}")`);
		this.userData.set(user.user, user);
		this.emit("userAdded", user, this.userData.get(user.user));
	}

	
	async addGuild(guild)
	{
		console.log("Adding guild to database.");
		var guildId = this.guildData.size;
		guildId++;
		this.provider.db.run(`INSERT INTO guildSettings(guild) VALUES("${guild.id}")`);
		var gd = new GuildData(guildId, guild.id, null, 0, null, null, 0, null, null, null, 500, {}, 0, this);
		this.guildData.set(guild.id, gd);
		this.emit("guildAdded", guild, gd);
	}

	async addGuildFromFile(guild)
	{
		console.log("Adding guild to database.");
		this.provider.db.run(`INSERT INTO guildSettings(guild, modChannel, mutedRole) VALUES("${guild.guild}", "${guild.modChannel}", "${guild.mutedRole}")`);
		this.guildData.set(guild.guild, guild);
		this.emit("guildAdded", guild, this.guildData.get(guild.guild));
	}
	
	async getAccolade(name)
	{
		var query = `SELECT * FROM accolades WHERE \`name\`="${name}"`;
		console.log(query);
		var accolade = await this.provider.db.all(query);
		return accolade[0];
	}
	
	async balanceTop(size)
	{
		var query = `SELECT user,money FROM userSettings ORDER BY \`money\` DESC LIMIT ${size}`
		return await this.provider.db.all(query);
	}
	
	async getGuild(guild)
	{
		var info = await this.provider.db.all("SELECT * FROM `guildSettings` WHERE `guild`="+guild.id);
		this.guildData.set(guild.id, new GuildData(info.id, info.guild, info.modChannel));
		return info;
	}
	
	async getUser(user)
	{
		var info = await this.provider.db.all("SELECT * FROM `userSettings` WHERE `user`="+user.id);
		this.userData.set(user.id, new UserData(info.id, info.user, info.prefix, info.money, info.dailyTime, this));
		return info;
	}

	async validRecord(id)
	{
		var valid = await this.fetchRecord(id);
		return valid != null;
	}

	async checkWarns(user, guild)
	{
		var results = await this.provider.db.all(`SELECT * FROM \`modRecord\` WHERE \`user\`="${user.id}" AND \`guild\`="${guild.id}" AND \`action\`="warn"`);
		return results.length;
	}

	async Mute(msg, time, reason)
	{
		await this.provider.db.run(`INSERT INTO \`muting\`(user, guild, time) VALUES(?, ?, ?)`, [msg.author.id, msg.guild.id, new Date().getTime() + time]);
		msg.member.addRole(msg.guild.data.muteRole);
		this.addRecord(msg.member, msg.guild, this.user, reason, "mute");
		this.ResumeMute(msg.member, msg.guild);
	}

	async ResumeMute(user, guild)
	{
		setTimeout(async () => {
			user.removeRole(guild.data.muteRole);
			await this.provider.db.run(`DELETE FROM \`muting\` WHERE \`user\`=?`, [user.id]);
			this.addRecord(user, guild, this.user, "Time expired", "unmute");
		}, new Date().getTime() - new Date(await this.GetMutingTime(user.id)).getTime());
	}

	async GetMutingTime(user)
	{
		return await this.provider.db.all(`SELECT * FROM \`muting\` WHERE \`user\`=?`, [user.id])[0];
	}

	async Warn(msg, reason)
	{
		var user = msg.author, channel = msg.channel, warns = await this.checkWarns(user, msg.guild), remaining = 5 - warns;

		var content = this.CreateEmbed(channel, reason, [{ title: "User In Question", description: msg.author, inline: true }, { title: "Warns Until Kick", description: `${remaining} Warns`, inline: true }], msg, "#FFA500", "Auto-Moderated By Starie");
		console.log(typeof content);

		if (typeof content == "string")
		{
			msg.channel.send(content).then((msg) => msg.delete(7000));
		}
		else
		{
			msg.channel.send({ embed : content }).then((msg) => msg.delete(7000));
		}

		await this.addRecord(msg.member, msg.guild, this.user, reason, "warn");

		return msg.delete(0);
	}

	async addRecord(user, guild, issuer, reason, action)
	{
		var query = `INSERT INTO \`modRecord\`(id, user, guild, issuer, reason, timeStamp, action) VALUES(${this.records+1}, "${user.id}", "${guild.id}", "${issuer.id}", "${reason.replace(/"/g, "&quot;")}", ${new Date().getTime()}, "${action}")`;
		console.log(query);
		this.provider.db.run(query);
		var data = this.guildData.get(guild.id);
		if (user.user) user = user.user;
		await this.fetchRecords();
		
		if (data.modChannel) 
		{
			const re = new Discord.RichEmbed();
			re.setAuthor(issuer.username, issuer.avatarURL);
            re.addField("User", user.tag);
            re.addField("Reason", reason);
            re.addField("Action", `${action.slice(0,1).toUpperCase()}${action.slice(1)}`);
            if (action == "ban") re.setColor("#FF0000"); else if (action == "kick" || action == "warn") re.setColor("#FFA500"); else re.setColor("#00F500");
            re.setFooter(`Case #${this.records}`);
            re.setTimestamp(new Date());
            this.channels.get(data.modChannel).send({ embed : re });
		}

		return;
	}

	async clearUser(user, guild)
	{
		await this.provider.db.get(`UPDATE \`modRecord\` SET \`cleared\`=1 WHERE \`user\`=${user.id} AND \`guild\`=${guild.id}`);
		return;
	}

	async fetchRecord(id)
	{
		var info = await this.provider.db.get("SELECT * FROM `modRecord` WHERE `id`="+id);
		return info;
	}

	async fetchUserRecords(user)
	{
		var info = await this.provider.db.all("SELECT * FROM `modRecord` WHERE `user`="+user.id);
		return info;		
	}

	async fetchGuildRecords(guild)
	{
		var info = await this.provider.db.all("SELECT * FROM `modRecord` WHERE `guild`="+guild.id);
		return info;
	}
}

function processPerms(permissions)
{
	console.log(permissions);
	var permsJSON = JSON.parse(permissions);
	var pa = [];
	
	for (var i in permsJSON)
	{
		console.log(permsJSON[i]);
		pa.push(new PermissionsGroup({ permissions: new Permissions({ all_perms: hasPerm("*", permsJSON[i]), view_logs: hasPerm("VIEW_LOGS", permsJSON[i]), delete_logs: hasPerm("DELETE_LOGS", permsJSON[i]), pin_logs: hasPerm("PIN_LOGS", permsJSON[i]), view_panel: hasPerm("VIEW_PANEL", permsJSON[i]) }) }));
	}
	
	return pa;
}

function hasPerm(perm, list)
{
	var contains = false;
	list.forEach((item) => {
		console.log(`${perm}: ${perm == item}`);
		
		if (!contains)
		{
			contains = perm == item;
		}
	});
	
	if (!contains) console.log("Doesn't contain");
	return contains;
}

module.exports = Client;