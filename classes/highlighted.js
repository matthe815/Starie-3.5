const Collection = require("discord.js").Collection;

class Highlighted {
    constructor(client)
    {
        this.client = client;
        this.words = new Collection();
    }

    add (word, member)
    {
        if (!this.words.has(word) || this.words.get(word).user != member.id)
        {
            return true;
        }

        return false;
    }

    query(word, message)
    {
        if (this.words.has(word))
        {
            this.words.owner.send(`You've been mentioned by the word ***${word}*** in the ${message.channel} in ${message.guild.name} by ${message.member.displayName}.`);
        }
    }
}