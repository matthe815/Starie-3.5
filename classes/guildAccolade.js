var Accolade = require("./accolade");

class GuildAccolade extends Accolade
{
	constructor(name, description, icon, conditions, client, guild, creator)
	{
		super(name, description, icon, conditions, client);
		this.guild = guild;
		this.creator = creator;
	}
}

module.exports = GuildAccolade;