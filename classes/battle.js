class Battle {
    constructor(data, client)
    {
        if (!data) return;
        this.client = client;
        this.id = data.id;
        this.opponent = {
            id: data.opponent.id,
            character: data.opponent.character,
            health: data.opponent.character.might,
            maxHealth: data.opponent.character.might,
            committed: false
        }
        this.challenger = {
            id: data.challenger.id,
            character: data.challenger.character,
            health: data.challenger.character.might,
            maxHealth: data.challenger.character.might,
            committed: false
        }
    }
}

module.exports = Battle;