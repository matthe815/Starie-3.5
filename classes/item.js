const Material = require('./material.js');

class Item {
    constructor(random=true)
    {
        this.name = "";
        this.might = 0;
        this.material = null;
        this.level = 0;
        this.type = "";
        this.price = 0;

        if (random)
        {
            let types = ["lefthand","righthand","helmet","chestplate","gloves","belt","pants","boots"];
            this.material = new Material();
            this.name = this.material.name;
            this.level = Math.floor(Math.random() * 15)+1;
            this.might = (this.material.might * this.level)+this.level;
            this.type = types[Math.floor(Math.random() * types.length)];
            this.price = (this.might + (this.level * 10) + this.name.length) / 2 + Math.floor(Math.random() * 10)+1;
            if (this.type == "lefthand") this.name = `${this.name} Sword`;
            if (this.type == "righthand") this.name = `${this.name} Shield`;
            if (this.type != "lefthand" && this.type != "righthand") this.name = `${this.name} ${this.type.slice(0, 1).toUpperCase()}${this.type.slice(1)}`;
        }
    }
}

module.exports = Item;