class Permissions {
	constructor(options = {})
	{
		console.log(options);
		this.all_perms = options.all_perms ? true : false;
		this.view_logs = options.view_logs ? true : false;
		this.view_panel = options.view_panel ? true : false;
		this.delete_logs = options.delete_logs ? true : false;
		this.pin_logs = options.pin_logs ? true : false;
	}
}

module.exports = Permissions;