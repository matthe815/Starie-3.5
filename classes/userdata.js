const Character = require('./rpgcharacter.js');

class UserData {
	constructor(id, user, prefix, panelAccess, money, dailyTime, client, character=null)
	{
		this.id = id;
		this.user = user;
		this.prefix = prefix;
		this.panelAccess = panelAccess;
		this.money = money;
		this.client = client;
		this.dailyTime = dailyTime;
		this.character = character;
		this.allowCalls = false;
		this.afk = false;
		this.afkReason = "";
	}

	async createCharacter(name, gender)
	{
		await this.client.provider.db.run(`INSERT INTO rpgCharacters(\`name\`, \`user\`, \`equipment\`, \`gender\`) VALUES("${name.replace(/"/g,"'")}", "${this.user}", "{}", "${gender}")`)
		this.character = new Character(this.client, this, name, gender);
		return true;
	}

	async saveCharacter()
	{
		await this.client.provider.db.run(`UPDATE \`rpgCharacters\` SET \`equipment\`='${this.character.stringifyEquipment()}';`);
	}

	async loadCharacter()
	{
		const char = await this.client.provider.db.get(`SELECT * FROM \`rpgCharacters\` WHERE \`user\`="${this.user}"`);
		if (!char) return;
		this.character = new Character(this.client, this);
		this.character.name = char.name;
		this.character.gender = char.gender;
		let items = JSON.parse(char.equipment);
		console.log(items);
		this.character.resolveEquipment(items);
	}
}

module.exports = UserData;