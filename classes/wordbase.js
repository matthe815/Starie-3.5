const EventEmitter = require('events');

class WordBase extends EventEmitter {
	constructor(client)
	{
		super();
		this._client = client;
	}
	
	async contains(value)
	{
		var words = await this._client.provider.db.all(`SELECT * FROM wordbase WHERE word="${safeString(value)}";`)
		var wordList = [];
		
		for (const word of words)
		{
			try {
				wordList.push(word);
			} catch (e) {
				console.log(e);
				continue;
			}
		}
		
		return !wordList.length == 0;
	}
	
	async speak(words)
	{
		var i=0;
		var sentence = [];
		while (i<words)
		{
			var wordLi = await this._client.provider.db.all(`SELECT * FROM wordbase WHERE position=${i};`);
			var wordList = [];
			
			for (const row of wordLi)
			{
				try {
					wordList.push(row.word);
				} catch (e) {
					console.log(e);
					continue;
				}
			}

			i++;
			var wordNum = Math.floor(Math.random() * wordList.length);
			console.log(wordNum);
			sentence.push(wordList[wordNum]);
		}

		return sentence.join(" ");
	}

	async learn(value, position)
	{
		var learn = await this._client.provider.db.run(`INSERT INTO wordbase(\`word\`, \`language\`, \`position\`) VALUES("${safeString(value)}", "English","${safeString(position)}")`);
		this.emit("wordLearned", value);
		return;
	}
}

function safeString(string)
{
	string = String(string);
	return string.replace(/"/g, "'");
}

module.exports = WordBase;